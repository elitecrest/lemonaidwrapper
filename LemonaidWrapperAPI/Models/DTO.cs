﻿using System;
using System.Collections.Generic;

namespace LemonaidWrapperAPI.Models
{

    public partial class AthleteDTO
    {
        public int Id { get; set; }
        public string Emailaddress { get; set; }
        public string UserName { get; set; }
        public int LoginType { get; set; }
        public string UniqId { get; set; }
    }

    public partial class AthleteSportsDTO
    {
        public int Id { get; set; }
        public int AthleteId { get; set; }
        public int SportId { get; set; }
        public string SportName { get; set; }
    }

    public partial class SportsDTO
    {
        public SportsDTO()
        {
            Id = 0;
            Name = string.Empty;
        }
           

        public int Id { get; set; }
        public string Name { get; set; }
        public string ThumbnailUrl { get; set; }
        public string SwitchingThumbnailUrl { get; set; }
        public URL urls { get; set; }
        public int Active { get; set; }
        public int AthleteId { get; set; }
        public int TeamType { get; set; }
    }

    public partial class URL
    {
        public string Test { get; set; }
        public string Staging { get; set; }
        public string Production { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property,
             Inherited = false,
             AllowMultiple = false)]
    internal sealed class OptionalAttribute : Attribute
    {
    }

    public partial class AthleteProfileDTO
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public int Gender { get; set; }
        public string School { get; set; }
        public string Emailaddress { get; set; }
        public string Description { get; set; }
        public double GPA { get; set; }
        public int Social { get; set; }
        public int SAT { get; set; }
        public int ACT { get; set; }
        public int InstituteId { get; set; }
        public string ProfilePicURL { get; set; }
        public string DOB { get; set; }
        public string Address { get; set; }
        public string FacebookId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int TotalCount { get; set; }
        public bool Liked { get; set; }
        public int FriendsInApp { get; set; }
        public int SharesApp { get; set; }
        public int AverageRating { get; set; }
        public int AcademicLevel { get; set; } //1-HighSchool 2-College
        public int YearLevel { get; set; } // 1- FR , 2-SO, 3-JR, 4-SR
        public int Class { get; set; }
        public int FriendsRate { get; set; }
        public int ShareRate { get; set; }
        public int SocialRate { get; set; }
        public int AcademicRate { get; set; }
        public int AtheleteRate { get; set; }

        public List<AthleteEventDTO> AtheleteBesttimes { get; set; }
        public string CoverPicURL { get; set; }
        public int AdType { get; set; }
        public int Premium { get; set; }
        public int CoachId { get; set; }
        public int AtheleteId { get; set; }
        public List<VideoDTO> AtheleteVideos { get; set; }
        [Optional]
        public string Height { get; set; }
        [Optional]
        public string Height2 { get; set; }
        [Optional]
        public string HeightType { get; set; }
        [Optional]
        public string Weight { get; set; }
        [Optional]
        public string WeightType { get; set; }
        [Optional]
        public string WingSpan { get; set; }
        [Optional]
        public string WingSpanType { get; set; }
        [Optional]
        public string ShoeSize { get; set; }
        [Optional]
        public string ShoeSizeType { get; set; }
        public string ClubName { get; set; }
        public string CoachName { get; set; }
        public string CoachEmailId { get; set; }
        public int AllAmerican { get; set; }
        public int AcademicAllAmerican { get; set; }
        public int Captain { get; set; }
        public int SwimswamTop { get; set; }
        public string Status { get; set; }
        public int AtheleteType { get; set; }
        public bool Active { get; set; }
        public string UniversityName { get; set; }
        public string UniversityProfilePic { get; set; }
        public string UniversityEmailaddress { get; set; }
        public string UniversityAddress { get; set; }
        public string UniversityPhoneNumber { get; set; }
        public string UniversitySize { get; set; }
        public string UniversityClassificationName { get; set; }
        public int MeasurementCount { get; set; }
        public string Major { get; set; }
        public string Phonenumber { get; set; }
        public int TotalLikedCount { get; set; }
        public bool Diamond { get; set; }
        public string UserName { get; set; }
        public int LoginType { get; set; }
        public string DeviceType { get; set; }
        public string DeviceId { get; set; }
        public int ClubId { get; set; }
        public bool RightSwiped { get; set; }
        public int ChatCount { get; set; }
        [Optional]
        public int SportId { get; set; }
        public string VerticalJump { get; set; }
        public string BroadJump { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public List<SportsDTO> AthleteSports { get; set; }
        public string SchoolCoachName { get; set; }
        public string SchoolCoachEmail { get; set; }
        public string SchoolZipCode { get; set; }
        public int GapyearLevel { get; set; } //Takingclass..
        public string GapyearDescription { get; set; }
        public string Country { get; set; }
        public string Strokes { get; set; }
        public int Location { get; set; }
        public int CountryId { get; set; }
        public double AP { get; set; }
        public bool isProfileCompleted { get; set; }
        public string _20YardShuttleRun { get; set; }
        public string _60YardShuttleRun { get; set; }
        public string KneelingMedBallToss { get; set; }
        public string RotationalMedBallToss { get; set; } 
        public int CityId { get; set; } 
        public int BasicStateId { get; set; }
        public string BasicStateName { get; set; }
        public string CityName { get; set; } 
        public int AthleteTransition { get; set; }

    }

    public partial class AthleteEventDTO
    {
        [Optional]
        public int AtheleteId { get; set; }
        public string Emailaddress { get; set; }
        public int EventTypeId { get; set; }
        public string BesttimeValue { get; set; }
        public string EventName { get; set; }
    }
    public partial class VideoDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string VideoURL { get; set; }
        public string ThumbnailURL { get; set; }
        public int Status { get; set; }
        public string AtheleteEmail { get; set; }
        public string VideoFormat { get; set; }
        public int Count { get; set; }
        [Optional]
        public int VideoType { get; set; }
        [Optional]
        public int VideoNumber { get; set; }
        [Optional]
        public int Duration { get; set; }
        [Optional]
        public int AtheleteId { get; set; }
    }


    public partial class UserDTO
    {
        public UserDTO()
        {
            this.DeviceId = string.Empty;
            this.DeviceType = string.Empty;
        }
        public int Id { get; set; }
        public string Emailaddress { get; set; }
        public string UserName { get; set; }
        public int LoginType { get; set; }
        public string UniqId { get; set; }
        public string UserType { get; set; } 
        public string DeviceType { get; set; }
        public string DeviceId { get; set; }
    }


    public partial class UserSportsDTO
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int SportId { get; set; }
        public string SportName { get; set; }
        public int Active { get; set; }
        public int AthleteId { get; set; }
        public string Name { get; set; }
        public string ThumbnailUrl { get; set; }
        public string SwitchingThumbnailUrl { get; set; }
        public URL urls { get; set; }


    }

    public partial class CoachProfileDTO
    {
        public string Phonenumber { get; set; }
        public string PreferredStrokes { get; set; }
        public string Description { get; set; }
        public int CoachId { get; set; }
        public string Emailaddress { get; set; }
        public bool HeadCoach { get; set; }
        [Optional]
        public int AcademicRate { get; set; }
        public bool payment_paid { get; set; }
        public int InstituteId { get; set; }
        [Optional]
        public object FacebookURL { get; set; }
        [Optional]
        public object TwitterURL { get; set; }
        [Optional]
        public object InstagramURL { get; set; }
        [Optional]
        public object YoutubeURL { get; set; }
        public List<SportsDTO> CoachSports { get; set; }
        public string Password { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public partial class CoachDTO
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string AreasInterested { get; set; }
        public string Address { get; set; }
        public string Emailaddress { get; set; }
        public string Description { get; set; }
        public int YearOfEstablish { get; set; }
        public int NCAA { get; set; }
        public int Conference { get; set; }
        public int AdmissionStandard { get; set; }
        public int AverageRating { get; set; }
        public string FacebookId { get; set; }
        public string ClassificationName { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string WebsiteURL { get; set; }
        public string FaidURL { get; set; }
        public string ApplURL { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string EnrollmentNo { get; set; }
        public string ApplicationNo { get; set; }
        public string AdmissionNo { get; set; }
        public string ProfilePicURL { get; set; }
        public string CoverPicURL { get; set; }
        public int TotalCount { get; set; }
        public bool Liked { get; set; }
        public int NoOfAthletesInApp { get; set; } //No of Athletes
        public int AcceptanceRate { get; set; }
        public int NCAARate { get; set; }
        public int ConferenceRate { get; set; }
        public int AdmissionRate { get; set; }
        public int SocialRate { get; set; }
        public int AtheleteRate { get; set; }
        //public DateTime FavDate { get; set; }
        //public DateTime InvitationDate { get; set; }
        public bool Accept { get; set; }
        public int TotalAtheleteAcceptedCount { get; set; }
        public int Cost { get; set; }
        public int MensTeam { get; set; }
        public int WomenTeam { get; set; }
        public int Size { get; set; }
        public int AdType { get; set; }
        public int CoachId { get; set; }
        public int AtheleteId { get; set; }
        public string AtheleteName { get; set; }
        public string AtheleteProfilePic { get; set; }
        public bool CoachActiveStatus { get; set; }
        public string Phonenumber { get; set; }
        public bool HeadCoach { get; set; }
        public string PreferredStrokes { get; set; }
        public int AcademicRate { get; set; }
        public bool Payment_Paid { get; set; }
        public bool Recommended { get; set; }
        public List<AthleteDTO> RecommendedAtheletes { get; set; }
        public bool RightSwiped { get; set; }
        public int ChatCount { get; set; }
        public string InstituteEmailaddress { get; set; }
        public bool RecommendedByFFF { get; set; }
        public int QuestionId { get; set; }
        public List<SportsDTO> CoachSports { get; set; }
        public string Question { get; set; } 
        public int SportId { get; set; }
        public string SportName { get; set; } 
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PlanId { get; set; }
        public string PlanName { get; set; }
        public string SubscriptionType { get; set; }
        public string Price { get; set; } 
        public string StripeUserId { get; set; }
        public string CardToken { get; set; } 
        public int TeamType { get; set; }
        public int Coach360VideosCnt { get; set; } 
        public string VirtualVideoURL { get; set; }
        public string MatchesVirtualVideoURL { get; set; }
        public string VirtualThumbnailURL { get; set; }
        public string MatchesVirtualThumbnailURL { get; set; }
    }


    public partial class ClubCoachDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Gender { get; set; }
        public string Emailaddress { get; set; }
        public string Description { get; set; }
        public string ProfilePicURL { get; set; }
        public string DOB { get; set; }
        public string ClubAddress { get; set; }
        public string FacebookId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string CoverPicURL { get; set; }
        public string ClubName { get; set; }
        public bool Active { get; set; }
        public string Phonenumber { get; set; }
        public string DeviceType { get; set; }
        public string DeviceId { get; set; }
        public int TeamType { get; set; }
        public string Location { get; set; }
        public string SchoolName { get; set; }
        public string ZipCode { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int SchoolType { get; set; }
        public int SportId { get; set; }
        public string ClubPhoneNumber { get; set; }
        public List<SportsDTO> ClubCoachSports { get; set; }
        public int LoginType { get; set; }
        public string Username { get; set; }
        public string Almamater { get; set; }
        public string Sport { get; set; }
    }


    public partial class FamilyFriendsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Gender { get; set; }
        public string Emailaddress { get; set; }
        public string Description { get; set; }
        public string ProfilePicURL { get; set; }
        public string DOB { get; set; }
        public string FacebookId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Phonenumber { get; set; }
        public string DeviceType { get; set; }
        public string DeviceId { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int SportId { get; set; }
        public List<SportsDTO> FamilyFriendsSports { get; set; }
        public int LoginType { get; set; }
        public string Username { get; set; }
        public string Almamater { get; set; }
        public string Sport { get; set; }
    }

    public class EmailDTO
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public int Status { get; set; }
        public string MailType { get; set; }
    }

    public partial class AthleteAdDTO
    {

        public string Emailaddress { get; set; }
        public int LoginType { get; set; }
        public int SportId { get; set; }
        public int AdId { get; set; }
        public bool Status { get; set; }

        public string UserType { get; set; }
    }

}


    public partial class ChatDTO
    {
        public string Emailaddress { get; set; }
        public int LoginType { get; set; }
        public int SportId { get; set; }
        public int AdId { get; set; } 
        public int SenderId { get; set; }
        public int ReceiverId { get; set; }
        public int SenderType { get; set; }
        public int ReceiverType { get; set; }
        public DateTime Date { get; set; }
        public string Message { get; set; }
        public int Offset { get; set; }
        public int Max { get; set; }
        public int Count { get; set; }
        public int SenderDBId { get; set; }
        public int ReceiverDBId { get; set; }
        public string UserType { get; set; }

    }

public partial class AdDTO
{ 
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public string ProfilePicURL { get; set; }
    public string VideoURL { get; set; }
    public int AdType { get; set; }
    public string WebsiteURL { get; set; }
    public int EndAdUserId { get; set; }
    public string EndAdUserName { get; set; }
    public string EndAdUserEmail { get; set; } 
    public int SportId { get; set; } 
    public string SportName { get; set; } 
    public AdChatCountDTO Count { get; set; } 
}

    public partial class EndUserDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Emailaddress { get; set; }
        public string Password { get; set; }    
        public int UserType { get; set; }
        public string CompanyName { get; set; }
        public string Phonenumber { get; set; }

    }

    public partial class AthleteandAdDTO
    {
        public int AdId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ProfilePicURL { get; set; }
        public string WebsiteURL { get; set; }
        public int AdType { get; set; }
        public int EndAdUserId { get; set; }
        public string Emailaddress { get; set; }
        public string Username { get; set; }
        public int LoginType { get; set; }
        public int SportId { get; set; }
        public int ChatCount { get; set; }
        public int AthleteId { get; set; }
        public string AthleteName { get; set; }
        public string AthleteProfilePic { get; set; }
        public string DeviceId { get; set; }
        public string DeviceType { get; set; } 
        public string UserType { get; set; }

    }

    public partial class SupportMailDTO
    {
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string SSL { get; set; }

    }

public partial class AdChatCountDTO
{
    public int CntAthletes { get; set; }
    public int CntFFF { get; set; }
    public int CntClC { get; set; }

}

public partial class Video360TypeDTO
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string VideoURL { get; set; }
    public string ThumbNailURL { get; set; }
    public int CoachId { get; set; }
    public int Status { get; set; }
    public string VideoFormat { get; set; }
    public int Duration { get; set; }
    public int VideoType { get; set; }
    public string Latitude { get; set; }
    public string Longitude { get; set; }
    public string Description { get; set; }
    public string Author { get; set; }
    public string Emailaddress { get; set; }
    
}

public partial class CoachVideo360
{
    public string SearchText { get; set; }
    public int Offset { get; set; }
    public int Max { get; set; }
}

