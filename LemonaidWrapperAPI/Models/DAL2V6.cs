﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using LemonaidWrapperAPI.Controllers;
using System.Text;
using System.Security.Cryptography;
using System.IO;


namespace LemonaidWrapperAPI.Models
{
    public class DAL2V6
    {
        internal static int AddAthleteChatAdMatch(AthleteAdDTO athleteAdDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAthleteChatAdMatch_2V6]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteId = cmd.Parameters.Add("@AthleteEmail", SqlDbType.VarChar, 100);
                    athleteId.Direction = ParameterDirection.Input;
                    athleteId.Value = athleteAdDTO.Emailaddress;

                    SqlParameter loginType = cmd.Parameters.Add("@LoginType", SqlDbType.Int);
                    loginType.Direction = ParameterDirection.Input;
                    loginType.Value = athleteAdDTO.LoginType;

                    SqlParameter adId = cmd.Parameters.Add("@ChatAdId", SqlDbType.Int);
                    adId.Direction = ParameterDirection.Input;
                    adId.Value = athleteAdDTO.AdId;

                    SqlParameter sportId = cmd.Parameters.Add("@SportId", SqlDbType.Int);
                    sportId.Direction = ParameterDirection.Input;
                    sportId.Value = athleteAdDTO.SportId;

                    SqlParameter status = cmd.Parameters.Add("@Status", SqlDbType.Bit);
                    status.Direction = ParameterDirection.Input;
                    status.Value = athleteAdDTO.Status;

                    SqlParameter UserType = cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 20);
                    UserType.Direction = ParameterDirection.Input;
                    UserType.Value = athleteAdDTO.UserType;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v6/AddAthleteChatAdMatch", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static int GetUserId(ChatDTO chatDTO)
        {
            int userId = 0;
            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUserId_2V6]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteId = cmd.Parameters.Add("@AthleteEmail", SqlDbType.VarChar, 100);
                    athleteId.Direction = ParameterDirection.Input;
                    athleteId.Value = chatDTO.Emailaddress;

                    SqlParameter loginType = cmd.Parameters.Add("@LoginType", SqlDbType.Int);
                    loginType.Direction = ParameterDirection.Input;
                    loginType.Value = chatDTO.LoginType;

                    SqlParameter UserType = cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 20);
                    UserType.Direction = ParameterDirection.Input;
                    UserType.Value = chatDTO.UserType;

                    userId = Convert.ToInt32(cmd.ExecuteScalar());


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v6/GetUserId", "", ex.Message, "Exception", 0);
                throw;
            }

            return userId;
        }


        internal static int DeleteChatAdAtheleteMatch(AthleteAdDTO athleteAdDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteChatAdAtheleteMatch_2V6]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteId = cmd.Parameters.Add("@AthleteEmail", SqlDbType.VarChar, 100);
                    athleteId.Direction = ParameterDirection.Input;
                    athleteId.Value = athleteAdDTO.Emailaddress;

                    SqlParameter loginType = cmd.Parameters.Add("@LoginType", SqlDbType.Int);
                    loginType.Direction = ParameterDirection.Input;
                    loginType.Value = athleteAdDTO.LoginType;

                    SqlParameter adId = cmd.Parameters.Add("@ChatAdId", SqlDbType.Int);
                    adId.Direction = ParameterDirection.Input;
                    adId.Value = athleteAdDTO.AdId;

                    SqlParameter sportId = cmd.Parameters.Add("@SportId", SqlDbType.Int);
                    sportId.Direction = ParameterDirection.Input;
                    sportId.Value = athleteAdDTO.SportId; 

                    SqlParameter UserType = cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 20);
                    UserType.Direction = ParameterDirection.Input;
                    UserType.Value = athleteAdDTO.UserType;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2v6/DeleteChatAdAtheleteMatch ", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static int AddChatAdSports(AdDTO adDTO)
        {
            int adId = 0;
            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddChatAdSports]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter AdIdPar = cmd.Parameters.Add("@AdId", SqlDbType.Int);
                    AdIdPar.Direction = ParameterDirection.Input;
                    AdIdPar.Value = adDTO.Id;

                    SqlParameter SportId = cmd.Parameters.Add("@SportId", SqlDbType.NVarChar, 1000);
                    SportId.Direction = ParameterDirection.Input;
                    SportId.Value = adDTO.SportId; 

                    SqlParameter IdPar = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    IdPar.Direction = ParameterDirection.InputOutput;
                    IdPar.Value = adDTO.Id;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        adId = (int)myData["Id"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V6/AddChatAdSports", "", ex.Message, "Exception", 0);
                throw;
            }

            return adId;
        }

        internal static AdChatCountDTO GetChatCountByChatID(int ChatAdid)
        {
            AdChatCountDTO adChatCountDTO = new AdChatCountDTO();


            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[ChatCountByChatID]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter ChatAdIdPar = cmd.Parameters.Add("@ChatAdId", SqlDbType.Int);
                    ChatAdIdPar.Direction = ParameterDirection.Input;
                    ChatAdIdPar.Value = ChatAdid;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        adChatCountDTO.CntAthletes = (int)myData["AthCount"];
                        adChatCountDTO.CntClC = (int)myData["ClcCount"];
                        adChatCountDTO.CntFFF = (int)myData["FFFCount"];
                    }


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v6/GetChatCountByChatID", "", ex.Message, "Exception", 0);
                throw;
            }

            return adChatCountDTO;
        }


        internal static List<AthleteandAdDTO> GetUsersForChatAdByAdId(int AdId, string UserType)
        {
            List<AthleteandAdDTO> athleteDTO = new List<AthleteandAdDTO>();
            User2v2Controller user2 = new User2v2Controller();
            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUsersForChatAdByAdId_2V6]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter AdIdPar = cmd.Parameters.Add("@AdId", SqlDbType.Int);
                    AdIdPar.Direction = ParameterDirection.Input;
                    AdIdPar.Value = AdId;

                    SqlParameter UserTypePar = cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 20);
                    UserTypePar.Direction = ParameterDirection.Input;
                    UserTypePar.Value = UserType;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteandAdDTO v = new AthleteandAdDTO();
                        v.AdId = (myData["ChatAdId"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["ChatAdId"]);
                        v.Name = (myData["Name"] == DBNull.Value) ? string.Empty : myData["Name"].ToString();
                        v.ProfilePicURL = (myData["ProfilePicURL"] == DBNull.Value) ? string.Empty : myData["ProfilePicURL"].ToString();
                        v.WebsiteURL = (myData["WebsiteURL"] == DBNull.Value) ? string.Empty : myData["WebsiteURL"].ToString();
                        v.Description = (myData["Description"] == DBNull.Value) ? string.Empty : myData["Description"].ToString();
                        v.AdType = (myData["Adtype"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["Adtype"]);
                        v.EndAdUserId = (myData["EndAdUserId"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["EndAdUserId"]);
                        v.SportId = (myData["SportId"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["SportId"]);
                        v.Emailaddress = (myData["Emailaddress"] == DBNull.Value) ? string.Empty : myData["Emailaddress"].ToString();
                        v.Username = (myData["Username"] == DBNull.Value) ? string.Empty : myData["Username"].ToString();
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["LoginType"]);
                        v.AthleteId = (myData["AthleteID"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AthleteID"]);
                        AthleteController ath = new AthleteController();
                        string email = string.Empty;
                        if (v.LoginType == 1 || v.LoginType == 4)
                        { email = v.Emailaddress; }
                        else { email = v.Username; }
                        if (UserType == UserTypes.HighSchooAthlete)
                        {
                            AthleteProfileDTO athleteProfile = ath.GetAthleteProfile(email, v.SportId, v.LoginType);
                            v.DeviceId = athleteProfile.DeviceId;
                            v.DeviceType = athleteProfile.DeviceType;
                            v.AthleteName = athleteProfile.Name;
                            v.AthleteProfilePic = athleteProfile.ProfilePicURL;
                        }
                        else
                            if (UserType == UserTypes.FamilyFriends)
                        {

                            FamilyFriendsDTO profile = (FamilyFriendsDTO)user2.GetFamilyFriendsProfile(email, v.SportId, v.LoginType);
                            v.DeviceId = profile.DeviceId;
                            v.DeviceType = profile.DeviceType;
                            v.AthleteName = profile.Name;
                            v.AthleteProfilePic = profile.ProfilePicURL;
                        }
                        else
                            if (UserType == UserTypes.ClubCoach)
                        {

                            ClubCoachDTO profile = (ClubCoachDTO)user2.GetClubCoachProfile(email, v.SportId, v.LoginType);
                            v.DeviceId = profile.DeviceId;
                            v.DeviceType = profile.DeviceType;
                            v.AthleteName = profile.Name;
                            v.AthleteProfilePic = profile.ProfilePicURL;
                        }
                        v.UserType = UserType;
                        athleteDTO.Add(v);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v6/GetUsersForChatAdByAdId", "", ex.Message, "Exception", 0);
                throw;
            }

            return athleteDTO;
        }
    }
}