﻿using System;
using System.Collections.Generic; 
using System.Net.Mail; 
using SendGrid;
using System.Net;
 

namespace LemonaidWrapperAPI.Models
{
    public class SendGridSMTPMail
    {
        public static int SendEmail(SendGridMailModel sendGridModel)
        {
            try
            {
                SupportMailDTO support = new SupportMailDTO();
                support = DAL2V4.GetSupportMails("SendGridMail");

                sendGridModel.APIKey = support.Host;
                var myMessage = new SendGridMessage();
                myMessage.From = new MailAddress(sendGridModel.fromMail, sendGridModel.fromName);
                myMessage.AddTo(sendGridModel.toMail);
                myMessage.Subject = sendGridModel.subject;
                myMessage.Html = "<p>" + sendGridModel.Message + "</p>";


                var credentials = new NetworkCredential(support.EmailAddress, support.Password);
                var transportWeb = new Web(credentials);
                transportWeb.DeliverAsync(myMessage);
                return 1;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }
        }
    }

    public class SendGridMailModel {
        public List<string> toMail { set; get; }
        public string fromName { set; get; }
        public string fromMail { set; get; }
        public string subject { set; get; }
        public string Message { set; get; }
        public string APIKey { set; get; }
        public string email { get; set; }
        public string password { get; set; }
    }
}