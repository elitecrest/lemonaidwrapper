﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LemonaidWrapperAPI.Models
{
    public class EmailLogModel
    {
        public int AddEmailLog(string @from, string to, string subjectorErr, string mailType, int status)
        {
            EmailDTO email = new EmailDTO
            {
                From = @from,
                To = to,
                Status = status,
                Subject = subjectorErr,
                MailType = mailType
            };
            DAL.AddEmailLog(email);
            return 1;
        }
    }
}