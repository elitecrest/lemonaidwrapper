﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;


namespace LemonaidWrapperAPI.Models
{
    public class DAL
    {
        public static SqlConnection ConnectToDb()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["LemonaidDB"].ConnectionString;
            var sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }
         
        internal static int AddAthlete(AthleteDTO athleteDto)
        {
            int AthId = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAthlete]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure; 
         

                    SqlParameter Emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = athleteDto.Emailaddress;
                      
                    SqlParameter Username = cmd.Parameters.Add("@Username", SqlDbType.VarChar, 500);
                    Username.Direction = ParameterDirection.Input;
                    Username.Value = string.IsNullOrWhiteSpace(athleteDto.UserName) ? string.Empty : athleteDto.UserName;

                    SqlParameter LoginType = cmd.Parameters.Add("@LoginType", SqlDbType.Int);
                    LoginType.Direction = ParameterDirection.Input;
                    LoginType.Value = (athleteDto.LoginType == null) ? 0 : athleteDto.LoginType;  

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    { 
                        AthId = (int)myData["id"];  
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return AthId;
        }


        internal static int AddAthleteSports(AthleteSportsDTO athleteSportsDto)
        {
            int Id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAthleteSports]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlParameter athleteId = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    athleteId.Direction = ParameterDirection.Input;
                    athleteId.Value = athleteSportsDto.AthleteId;

                    SqlParameter sportId = cmd.Parameters.Add("@SportId", SqlDbType.Int);
                    sportId.Direction = ParameterDirection.Input;
                    sportId.Value = athleteSportsDto.SportId;
 
                    SqlParameter IdPar = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    IdPar.Direction = ParameterDirection.Output;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        Id = (int)myData["id"];
                    
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return Id;
        }


        internal static List<SportsDTO> GetSports()
        {

            List<SportsDTO> sports = new List<SportsDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetSports]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure; 

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        SportsDTO sportsDto = new SportsDTO();
                        sportsDto.Id = (int)myData["Id"];
                        sportsDto.Name = (myData["Name"] == DBNull.Value) ? string.Empty : (string)myData["Name"];
                        sportsDto.ThumbnailUrl = (myData["ThumbnailUrl"] == DBNull.Value) ? string.Empty : (string)myData["ThumbnailUrl"];
                        sportsDto.SwitchingThumbnailUrl = (myData["SwitchingURL"] == DBNull.Value) ? string.Empty : (string)myData["SwitchingURL"];
                        sportsDto.TeamType = (myData["TeamType"] == DBNull.Value) ? 0: (int)myData["TeamType"];
                        URL url = new URL();
                        url = GetURLsForSports(sportsDto.Id);
                        sportsDto.urls = url;
                        sports.Add(sportsDto);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("WrapperDal/GetSports", "", e.Message, "Exception", 0);
            }

            return sports;
        }

        internal static List<AthleteSportsDTO> GetAthleteSports(string emailaddress, int logintype)
        {

            List<AthleteSportsDTO> sports = new List<AthleteSportsDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteSports]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter EmailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    EmailaddressPar.Direction = ParameterDirection.Input;
                    EmailaddressPar.Value = emailaddress;

                    SqlParameter logintypePar = cmd.Parameters.Add("@logintype", SqlDbType.Int);
                    logintypePar.Direction = ParameterDirection.Input;
                    logintypePar.Value = logintype;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteSportsDTO sportsDto = new AthleteSportsDTO();
                        sportsDto.Id = (int)myData["Id"];
                        sportsDto.AthleteId = (myData["UserId"] == DBNull.Value) ? 0 : (int)myData["UserId"];
                        sportsDto.SportId = (myData["SportId"] == DBNull.Value) ? 0 : (int)myData["SportId"]; 
                        sports.Add(sportsDto);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("WrapperDal/GetAthleteSports", "", e.Message, "Exception", 0);
            }

            return sports;
        }

        internal static List<SportsDTO> GetSportsFromEmail(string emailaddress, int loginType, string userType)
        {
            List<SportsDTO> sports = new List<SportsDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetSportsFromEmail]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlParameter logintypePar = cmd.Parameters.Add("@logintype", SqlDbType.Int);
                    logintypePar.Direction = ParameterDirection.Input;
                    logintypePar.Value = loginType;

                    SqlParameter userTypePar = cmd.Parameters.Add("@usertype", SqlDbType.VarChar, 50);
                    userTypePar.Direction = ParameterDirection.Input;
                    userTypePar.Value = userType;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        SportsDTO sportsDto = new SportsDTO();
                        sportsDto.Id = (int)myData["Id"];
                        sportsDto.Active = (myData["Active"] == DBNull.Value) ? 0 : (int)myData["Active"];
                        sportsDto.Name = (myData["Name"] == DBNull.Value) ? string.Empty : (string)myData["Name"];
                        sportsDto.ThumbnailUrl = (myData["ThumbnailUrl"] == DBNull.Value) ? string.Empty : (string)myData["ThumbnailUrl"];
                        sportsDto.SwitchingThumbnailUrl = (myData["SwitchingURL"] == DBNull.Value) ? string.Empty : (string)myData["SwitchingURL"];
                        URL url = new URL();
                        url = GetURLsForSports(sportsDto.Id);
                        sportsDto.urls = url;
                        sports.Add(sportsDto);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("WrapperDal/GetSportsFromEmail", "", e.Message, "Exception", 0);
            }

            return sports;
        }



        internal static int AddUser(UserDTO UserDto)
        {
            int AthId = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddUser]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlParameter emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    emailaddress.Direction = ParameterDirection.Input;
                    emailaddress.Value = UserDto.Emailaddress;

                    SqlParameter username = cmd.Parameters.Add("@Username", SqlDbType.VarChar, 500);
                    username.Direction = ParameterDirection.Input;
                    username.Value = string.IsNullOrWhiteSpace(UserDto.UserName) ? string.Empty : UserDto.UserName;

                    SqlParameter loginType = cmd.Parameters.Add("@LoginType", SqlDbType.Int);
                    loginType.Direction = ParameterDirection.Input;
                    loginType.Value = (UserDto.LoginType == null) ? 0 : UserDto.LoginType;

                    SqlParameter userType = cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 50);
                    userType.Direction = ParameterDirection.Input;
                    userType.Value = UserDto.UserType; 

                    SqlParameter deviceId = cmd.Parameters.Add("@DeviceId", SqlDbType.VarChar, 500);
                    deviceId.Direction = ParameterDirection.Input;
                    deviceId.Value = UserDto.DeviceId;

                    SqlParameter deviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 100);
                    deviceType.Direction = ParameterDirection.Input;
                    deviceType.Value = UserDto.DeviceType;

                    SqlParameter id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    id.Direction = ParameterDirection.Output;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthId = (int)myData["id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("WrapperDal/AddUser", "", e.Message, "Exception", 0);
            }

            return AthId;
        }


        internal static  List<SportsDTO> AddUserSports(UserSportsDTO userSportsDto)
        {
          List<SportsDTO> sports = new List<SportsDTO>();
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddUserSports]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlParameter userId = cmd.Parameters.Add("@UserId", SqlDbType.Int);
                    userId.Direction = ParameterDirection.Input;
                    userId.Value = userSportsDto.UserId;

                    SqlParameter sportId = cmd.Parameters.Add("@SportId", SqlDbType.Int);
                    sportId.Direction = ParameterDirection.Input;
                    sportId.Value = userSportsDto.SportId; 

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    { 
                       SportsDTO sportsDto = new SportsDTO(); 
                        sportsDto.Id = (myData["SportId"] == DBNull.Value) ? 0 : (int)myData["SportId"];
                        sportsDto.Active = (myData["Active"] == DBNull.Value) ? 0 : (int)myData["Active"];
                        sportsDto.Name = (myData["Name"] == DBNull.Value) ? string.Empty : (string)myData["Name"];
                        sportsDto.ThumbnailUrl = (myData["ThumbnailUrl"] == DBNull.Value) ? string.Empty : (string)myData["ThumbnailUrl"];
                        sportsDto.SwitchingThumbnailUrl = (myData["SwitchingUrl"] == DBNull.Value) ? string.Empty : (string)myData["SwitchingUrl"];
                         URL url = new URL();
                         url = GetURLsForSports(sportsDto.Id);
                        sportsDto.urls = url;
                        sports.Add(sportsDto);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("WrapperDal/AddUserSports", "", e.Message, "Exception", 0);
            }

            return sports;
        }


        internal static List<SportsDTO> GetUserSports(string emailaddress, int logintype,string userType)
        {

            List<SportsDTO> sports = new List<SportsDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUserSports]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlParameter logintypePar = cmd.Parameters.Add("@logintype", SqlDbType.Int);
                    logintypePar.Direction = ParameterDirection.Input;
                    logintypePar.Value = logintype;
                        
                    SqlParameter userTypePar = cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 20);
                    userTypePar.Direction = ParameterDirection.Input;
                    userTypePar.Value = userType;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        SportsDTO sportsDto = new SportsDTO(); 
                        sportsDto.Id = (myData["SportId"] == DBNull.Value) ? 0 : (int)myData["SportId"];
                        sportsDto.Active = (myData["Active"] == DBNull.Value) ? 0 : (int)myData["Active"];
                        sportsDto.Name = (myData["Name"] == DBNull.Value) ? string.Empty : (string)myData["Name"];
                        sportsDto.ThumbnailUrl = (myData["ThumbnailUrl"] == DBNull.Value) ? string.Empty : (string)myData["ThumbnailUrl"];
                        sportsDto.SwitchingThumbnailUrl = (myData["SwitchingUrl"] == DBNull.Value) ? string.Empty : (string)myData["SwitchingUrl"];
                        URL url = new URL();
                        url = GetURLsForSports(sportsDto.Id);
                        sportsDto.urls = url;
                        sports.Add(sportsDto);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("WrapperDal/GetUserSports", "", e.Message, "Exception", 0);
            }

            return sports;
        }


        internal static URL GetURLsForSports(int sportId)
        {
            URL url = new URL();
            if (sportId == 1) //Swimming
            {
                url.Test = "http://lemonaidbizapitest.azurewebsites.net/";
                url.Staging = "http://lemonaidstagingapi.azurewebsites.net/";
                url.Production = "http://lemonaidbizapi.azurewebsites.net/";
            }
            else if (sportId == 2) //Rowing
            {
                url.Test = "http://lemonaidrowingapitest.azurewebsites.net/";
                url.Staging = "http://lemonaidrowingapistaging.azurewebsites.net/";
                url.Production = "http://lemonaidrowingapi.azurewebsites.net/";
            }
            else if (sportId == 3) //Diving
            {
                url.Test = "http://lemonaiddivingapitest.azurewebsites.net/";
                url.Staging = "http://lemonaiddivingapistaging.azurewebsites.net/";
                url.Production = "http://lemonaiddivingapi.azurewebsites.net/";
            }
            else if (sportId == 4) //Tennis
            {
                url.Test = "http://lemonaidtennisapistaging.azurewebsites.net/";
                url.Staging = "http://lemonaidtennisapistaging.azurewebsites.net/";
                url.Production = "http://lemonaidtennisapi.azurewebsites.net/";
            }
            else if (sportId == 5) //Track
            {
                url.Test = "http://lemonaidtrackapistaging.azurewebsites.net/";
                url.Staging = "http://lemonaidtrackapistaging.azurewebsites.net/";
                url.Production = "http://lemonaidtrackapi.azurewebsites.net/";
            }
            else if (sportId == 6) //Lacrosse
            {
                url.Test = "http://lemonaidlacrosseapistaging.azurewebsites.net/";
                url.Staging = "http://lemonaidlacrosseapistaging.azurewebsites.net/";
                url.Production = "http://lemonaidlacrosseapi.azurewebsites.net/";
            }
            else if (sportId == 7) //Golf
            {
                url.Test = "http://lemonaidgolfapistaging.azurewebsites.net/";
                url.Staging = "http://lemonaidgolfapistaging.azurewebsites.net/";
                url.Production = "http://lemonaidgolfapi.azurewebsites.net/";
            }
            else if (sportId == 8) //Water Polo
            {
                url.Test = "http://lemonaidwaterpoloapistaging.azurewebsites.net/";
                url.Staging = "http://lemonaidwaterpoloapistaging.azurewebsites.net/";
                url.Production = "http://lemonaidwaterpoloapi.azurewebsites.net/";
            }
            else if (sportId == 9) //Football
            {
                url.Test = "http://Lemonaidfootballapistaging.azurewebsites.net/";
                url.Staging = "http://Lemonaidfootballapistaging.azurewebsites.net/";
                url.Production = "http://Lemonaidfootballapi.azurewebsites.net/";
            }
            else if (sportId == 10) //Basketball
            {
                url.Test = "http://lemonaidbasketballapistaging.azurewebsites.net/";
                url.Staging = "http://lemonaidbasketballapistaging.azurewebsites.net/";
                url.Production = "http://lemonaidbasketballapi.azurewebsites.net/";
            }
            else if (sportId == 11) //Baseball
            {
                url.Test = "http://lemonaidbaseballapistaging.azurewebsites.net/";
                url.Staging = "http://lemonaidbaseballapistaging.azurewebsites.net/";
                url.Production = "http://lemonaidbaseballapi.azurewebsites.net/";
            }
            else if (sportId == 12) //Soccer
            {
                url.Test = "http://lemonaidsoccerapistaging.azurewebsites.net/";
                url.Staging = "http://lemonaidsoccerapistaging.azurewebsites.net/";
                url.Production = "http://lemonaidsoccerapi.azurewebsites.net/";
            }
            else if (sportId == 13) //Softball
            {
                url.Test = "http://lemonaidsoftballapistaging.azurewebsites.net/";
                url.Staging = "http://lemonaidsoftballapistaging.azurewebsites.net/";
                url.Production = "http://lemonaidsoftballapi.azurewebsites.net/";
            }
            else if (sportId == 14) //Wrestling 
            {
                url.Test = "http://lemonaidwrestlingapistaging.azurewebsites.net/";
                url.Staging = "http://lemonaidwrestlingapistaging.azurewebsites.net/";
                url.Production = "http://lemonaidwrestlingapi.azurewebsites.net/";
            }
            else if (sportId == 15) //IceHockey
            {
                url.Test = "http://lemonaidicehockeyapistaging.azurewebsites.net/";
                url.Staging = "http://lemonaidicehockeyapistaging.azurewebsites.net/";
                url.Production = "http://lemonaidicehockeyapi.azurewebsites.net/";
            }
            else if (sportId == 16) //Volleyball
            {
                url.Test = "http://lemonaidvolleyballapistaging.azurewebsites.net/";
                url.Staging = "http://lemonaidvolleyballapistaging.azurewebsites.net/";
                url.Production = "http://lemonaidvolleyballapi.azurewebsites.net/";
            }
            return url;
        }

        internal static int DeleteUserFromWrapper(string emailaddress, int sportId, int loginType, string userType)
        {
            int id = 0;

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteUserFromWrapper]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlParameter sportIdPar = cmd.Parameters.Add("@sportid", SqlDbType.Int);
                    sportIdPar.Direction = ParameterDirection.Input;
                    sportIdPar.Value = sportId;

                    SqlParameter logintypePar = cmd.Parameters.Add("@logintype", SqlDbType.Int);
                    logintypePar.Direction = ParameterDirection.Input;
                    logintypePar.Value = loginType;

                    SqlParameter userTypePar = cmd.Parameters.Add("@usertype", SqlDbType.VarChar, 50);
                    userTypePar.Direction = ParameterDirection.Input;
                    userTypePar.Value = userType;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("WrapperDal/DeleteUserFromWrapper", "", e.Message, "Exception", 0);
            }

            return id;
        }



        internal static void Logout(string emailaddress, int sportId, int logintype, string userType)
        { 
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[Logout]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlParameter sportIdPar = cmd.Parameters.Add("@sportId", SqlDbType.Int);
                    sportIdPar.Direction = ParameterDirection.Input;
                    sportIdPar.Value = sportId; 

                    SqlParameter logintypePar = cmd.Parameters.Add("@logintype", SqlDbType.Int);
                    logintypePar.Direction = ParameterDirection.Input;
                    logintypePar.Value = logintype;

                    SqlParameter userTypePar = cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 20);
                    userTypePar.Direction = ParameterDirection.Input;
                    userTypePar.Value = userType;

                    int Id = cmd.ExecuteNonQuery();  
                   
                    myConn.Close();
                }
            }
            catch (Exception e)
            { 
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("WrapperDal/Logout", "", e.Message, "Exception", 0);
            }

             
        }

        internal static int AddEmailLog(EmailDTO emailDTO)
        {
            int id = 0;
            SqlConnection myConn = ConnectToDb();

            try
            {
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddEmailLog]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter from = cmd.Parameters.Add("@From", SqlDbType.VarChar, 1000);
                    from.Direction = ParameterDirection.Input;
                    from.Value = emailDTO.From;

                    SqlParameter to = cmd.Parameters.Add("@To", SqlDbType.VarChar, 1000);
                    to.Direction = ParameterDirection.Input;
                    to.Value = emailDTO.To;

                    SqlParameter subject = cmd.Parameters.Add("@Subject", SqlDbType.VarChar, 1000);
                    subject.Direction = ParameterDirection.Input;
                    subject.Value = emailDTO.Subject;

                    SqlParameter status = cmd.Parameters.Add("@Status", SqlDbType.Int);
                    status.Direction = ParameterDirection.Input;
                    status.Value = emailDTO.Status;

                    SqlParameter mailType = cmd.Parameters.Add("@MailType", SqlDbType.VarChar, 50);
                    mailType.Direction = ParameterDirection.Input;
                    mailType.Value = emailDTO.MailType;

                    SqlDataReader myData = cmd.ExecuteReader();
                    while (myData.Read())
                    {

                        id = (int)myData["Id"];
                    }
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddEmailLog", "", ex.Message, "Exception", 0);
            }
            return id;
        }

        internal static List<SportsDTO> GetAllSports()
        {

            List<SportsDTO> sports = new List<SportsDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAllSports]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        SportsDTO sportsDto = new SportsDTO();
                        sportsDto.Id = (int)myData["Id"];
                        sportsDto.Name = (myData["Name"] == DBNull.Value) ? string.Empty : (string)myData["Name"];
                        sportsDto.ThumbnailUrl = (myData["ThumbnailUrl"] == DBNull.Value) ? string.Empty : (string)myData["ThumbnailUrl"];
                        sportsDto.SwitchingThumbnailUrl = (myData["SwitchingURL"] == DBNull.Value) ? string.Empty : (string)myData["SwitchingURL"];
                        sportsDto.TeamType = (myData["TeamType"] == DBNull.Value) ? 0 : (int)myData["TeamType"];
                        URL url = new URL();
                        url = GetURLsForSports(sportsDto.Id);
                        sportsDto.urls = url;
                        sports.Add(sportsDto);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("WrapperDal/GetSports", "", e.Message, "Exception", 0);
            }

            return sports;
        }
    }
}