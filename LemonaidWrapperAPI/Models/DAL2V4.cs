﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using LemonaidWrapperAPI.Controllers;
using System.Text;
using System.Security.Cryptography;
using System.IO;


namespace LemonaidWrapperAPI.Models
{
    public class DAL2V4
    {
        internal static int AddAthleteChatAdMatch(AthleteAdDTO athleteAdDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAthleteChatAdMatch]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteId = cmd.Parameters.Add("@AthleteEmail", SqlDbType.VarChar,100);
                    athleteId.Direction = ParameterDirection.Input;
                    athleteId.Value = athleteAdDTO.Emailaddress;

                    SqlParameter loginType = cmd.Parameters.Add("@LoginType", SqlDbType.Int);
                    loginType.Direction = ParameterDirection.Input;
                    loginType.Value = athleteAdDTO.LoginType;

                    SqlParameter adId = cmd.Parameters.Add("@ChatAdId", SqlDbType.Int);
                    adId.Direction = ParameterDirection.Input;
                    adId.Value = athleteAdDTO.AdId;

                    SqlParameter sportId = cmd.Parameters.Add("@SportId", SqlDbType.Int);
                    sportId.Direction = ParameterDirection.Input;
                    sportId.Value = athleteAdDTO.SportId;

                    SqlParameter status = cmd.Parameters.Add("@Status", SqlDbType.Bit);
                    status.Direction = ParameterDirection.Input;
                    status.Value = athleteAdDTO.Status;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v4/AddAthleteChatAdMatch", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

 
        internal static int DeleteChatAdAtheleteMatch(AthleteAdDTO athleteAdDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteChatAdAtheleteMatch]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteId = cmd.Parameters.Add("@AthleteEmail", SqlDbType.VarChar, 100);
                    athleteId.Direction = ParameterDirection.Input;
                    athleteId.Value = athleteAdDTO.Emailaddress;

                    SqlParameter loginType = cmd.Parameters.Add("@LoginType", SqlDbType.Int);
                    loginType.Direction = ParameterDirection.Input;
                    loginType.Value = athleteAdDTO.LoginType;

                    SqlParameter adId = cmd.Parameters.Add("@ChatAdId", SqlDbType.Int);
                    adId.Direction = ParameterDirection.Input;
                    adId.Value = athleteAdDTO.AdId;

                    SqlParameter sportId = cmd.Parameters.Add("@SportId", SqlDbType.Int);
                    sportId.Direction = ParameterDirection.Input;
                    sportId.Value = athleteAdDTO.SportId;


                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2v4/DeleteChatAdAtheleteMatch", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static int AddAthleteAdChat(ChatDTO chatDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAthleteChatAdChat]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter SenderId = cmd.Parameters.Add("@SenderId", SqlDbType.Int);
                    SenderId.Direction = ParameterDirection.Input;
                    SenderId.Value = chatDTO.SenderDBId;

                    SqlParameter ReceiverId = cmd.Parameters.Add("@ReceiverId", SqlDbType.Int);
                    ReceiverId.Direction = ParameterDirection.Input;
                    ReceiverId.Value = chatDTO.ReceiverDBId;

                    SqlParameter SenderType = cmd.Parameters.Add("@SenderType", SqlDbType.Int);
                    SenderType.Direction = ParameterDirection.Input;
                    SenderType.Value = chatDTO.SenderType;

                    SqlParameter ReceiverType = cmd.Parameters.Add("@ReceiverType", SqlDbType.Int);
                    ReceiverType.Direction = ParameterDirection.Input;
                    ReceiverType.Value = chatDTO.ReceiverType; 

                    SqlParameter Message = cmd.Parameters.Add("@Message", SqlDbType.NVarChar, 4000);
                    Message.Direction = ParameterDirection.Input;
                    Message.Value = chatDTO.Message;


                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v4/AddAthleteAdChat", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static List<ChatDTO> GetAthleteChatAdChatMessages(ChatDTO chatDTO)
        {

            List<ChatDTO> chats = new List<ChatDTO>();

            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteChatAdChatMessages]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlParameter SenderId = cmd.Parameters.Add("@SenderId", SqlDbType.Int);
                    SenderId.Direction = ParameterDirection.Input;
                    SenderId.Value = chatDTO.SenderDBId;

                    SqlParameter ReceiverId = cmd.Parameters.Add("@ReceiverId", SqlDbType.Int);
                    ReceiverId.Direction = ParameterDirection.Input;
                    ReceiverId.Value = chatDTO.ReceiverDBId;

                    SqlParameter SenderType = cmd.Parameters.Add("@SenderType", SqlDbType.Int);
                    SenderType.Direction = ParameterDirection.Input;
                    SenderType.Value = chatDTO.SenderType;

                    SqlParameter ReceiverType = cmd.Parameters.Add("@ReceiverType", SqlDbType.Int);
                    ReceiverType.Direction = ParameterDirection.Input;
                    ReceiverType.Value = chatDTO.ReceiverType;

                    SqlParameter Date = cmd.Parameters.Add("@Date", SqlDbType.DateTime);
                    Date.Direction = ParameterDirection.Input;
                    Date.Value = (chatDTO.Date == null) ? Convert.ToDateTime("1900-01-01") : chatDTO.Date;


                    SqlParameter offset = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offset.Direction = ParameterDirection.Input;
                    offset.Value = chatDTO.Offset;

                    SqlParameter max = cmd.Parameters.Add("@max", SqlDbType.Int);
                    max.Direction = ParameterDirection.Input;
                    max.Value = chatDTO.Max;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ChatDTO v = new ChatDTO();  
                        v.SenderId = (myData["SenderId"] == DBNull.Value) ? 0 : (int)myData["SenderId"];
                        v.ReceiverId = (myData["ReceiverId"] == DBNull.Value) ? 0 : (int)myData["ReceiverId"];  
                        v.SenderType = (myData["SenderType"] == DBNull.Value) ? 0 : (int)myData["SenderType"];
                        v.ReceiverType = (myData["ReceiverType"] == DBNull.Value) ? 0 : (int)myData["ReceiverType"]; 
                        v.Date = (myData["Date"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["Date"];
                        v.Message = (myData["Message"] == DBNull.Value) ? "" : (string)myData["Message"];

                        chats.Add(v);
                    }
                    foreach (var c in chats)
                    {
                        c.Count = chats.Count;
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v4/GetAthleteChatAdChatMessages", "", ex.Message, "Exception", 0);
                throw;
            }

            return chats;
        }


        internal static List<AthleteandAdDTO> GetAthletesForChatAd(int EndAdUserId,int AdType)
        {
            List<AthleteandAdDTO> athleteDTO = new List<AthleteandAdDTO>();

            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthletesForChatAd]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter endAdUserIdPar = cmd.Parameters.Add("@EndAdUserId", SqlDbType.Int);
                    endAdUserIdPar.Direction = ParameterDirection.Input;
                    endAdUserIdPar.Value = EndAdUserId;

                    SqlParameter adTypePar = cmd.Parameters.Add("@AdType", SqlDbType.Int);
                    adTypePar.Direction = ParameterDirection.Input;
                    adTypePar.Value = AdType;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteandAdDTO v = new AthleteandAdDTO();
                        v.AdId = (myData["ChatAdId"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["ChatAdId"]);
                        v.Name = (myData["Name"] == DBNull.Value) ? string.Empty : myData["Name"].ToString();
                        v.ProfilePicURL = (myData["ProfilePicURL"] == DBNull.Value) ? string.Empty : myData["ProfilePicURL"].ToString();
                        v.WebsiteURL = (myData["WebsiteURL"] == DBNull.Value) ? string.Empty : myData["WebsiteURL"].ToString();
                        v.Description = (myData["Description"] == DBNull.Value) ? string.Empty : myData["Description"].ToString();
                        v.AdType = (myData["Adtype"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["Adtype"]) ;
                        v.EndAdUserId = (myData["EndAdUserId"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["EndAdUserId"]);
                        v.SportId = (myData["SportId"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["SportId"]);
                        v.Emailaddress = (myData["Emailaddress"] == DBNull.Value) ? string.Empty : myData["Emailaddress"].ToString();
                        v.Username = (myData["Username"] == DBNull.Value) ? string.Empty : myData["Username"].ToString();
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["LoginType"]);
                        v.AthleteId = (myData["AthleteID"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AthleteID"]);
                        AthleteController ath = new AthleteController();
                        string email = string.Empty;
                        if (v.LoginType == 1 || v.LoginType == 4)
                        { email = v.Emailaddress; }
                        else { email = v.Username; }
                        AthleteProfileDTO athleteProfile = ath.GetAthleteProfile(email, v.SportId, v.LoginType);
                        v.DeviceId = athleteProfile.DeviceId;
                        v.DeviceType = athleteProfile.DeviceType;
                        v.AthleteName = athleteProfile.Name;
                        v.AthleteProfilePic = athleteProfile.ProfilePicURL;

                        athleteDTO.Add(v);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v4/GetAthletesForChatAd", "", ex.Message, "Exception", 0);
                throw;
            }

            return athleteDTO;
        }

        internal static int GetAthleteChatAdChatMessagesUnreadCount(int senderId, int receiverId)
        {

            int Count = 0;

            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteChatAdChatMessagesUnreadCount]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter SenderIdPar = cmd.Parameters.Add("@SenderId", SqlDbType.Int);
                    SenderIdPar.Direction = ParameterDirection.Input;
                    SenderIdPar.Value = senderId;

                    SqlParameter ReceiverIdPar = cmd.Parameters.Add("@ReceiverId", SqlDbType.Int);
                    ReceiverIdPar.Direction = ParameterDirection.Input;
                    ReceiverIdPar.Value = receiverId;

                    Count = Convert.ToInt16(cmd.ExecuteScalar());

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V4/GetAthleteChatAdChatMessagesUnreadCount", "", ex.Message, "Exception", 0);
            }

            return Count;
        }


        internal static int GetUserId(ChatDTO chatDTO)
        {
            int userId = 0;
            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUserId]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteId = cmd.Parameters.Add("@AthleteEmail", SqlDbType.VarChar, 100);
                    athleteId.Direction = ParameterDirection.Input;
                    athleteId.Value = chatDTO.Emailaddress;

                    SqlParameter loginType = cmd.Parameters.Add("@LoginType", SqlDbType.Int);
                    loginType.Direction = ParameterDirection.Input;
                    loginType.Value = chatDTO.LoginType;

                    //SqlParameter sportId = cmd.Parameters.Add("@SportId", SqlDbType.Int);
                    //sportId.Direction = ParameterDirection.Input;
                    //sportId.Value = chatDTO.SportId;

                    userId = Convert.ToInt32(cmd.ExecuteScalar());
                 

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v4/GetUserId", "", ex.Message, "Exception", 0);
                throw;
            }

            return userId;
        }



        internal static EndUserDTO GetEndUserId(string Name)
        {
            EndUserDTO v = new EndUserDTO();

            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetEndUserId]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter namePar = cmd.Parameters.Add("@Name", SqlDbType.VarChar,100);
                    namePar.Direction = ParameterDirection.Input;
                    namePar.Value = Name ;
                     
                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        
                        v.Id = (myData["Id"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["Id"]);
                        v.Name = (myData["Name"] == DBNull.Value) ? string.Empty : myData["Name"].ToString();
                        v.Emailaddress = (myData["Emailaddress"] == DBNull.Value) ? string.Empty : myData["Emailaddress"].ToString();
                        v.Password = (myData["Password"] == DBNull.Value) ? string.Empty : myData["Password"].ToString();  

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v4/GetEndUserId", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }


        internal static int AddEndUser(EndUserDTO endUserDTO)
        {
            int AthId = 0;
            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddEndAdUser]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlParameter emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    emailaddress.Direction = ParameterDirection.Input;
                    emailaddress.Value = endUserDTO.Emailaddress;

                    SqlParameter password = cmd.Parameters.Add("@Password", SqlDbType.VarChar, 50);
                    password.Direction = ParameterDirection.Input;
                    password.Value = string.IsNullOrWhiteSpace(endUserDTO.Password) ? string.Empty : Encrypt(endUserDTO.Password);

                    SqlParameter adminName = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 50);
                    adminName.Direction = ParameterDirection.Input;
                    adminName.Value = (endUserDTO.Name == null) ? string.Empty : endUserDTO.Name;

                    SqlParameter userType = cmd.Parameters.Add("@UserType", SqlDbType.Int);
                    userType.Direction = ParameterDirection.Input;
                    userType.Value = endUserDTO.UserType;

                    SqlParameter companyName = cmd.Parameters.Add("@CompanyName", SqlDbType.VarChar, 100);
                    companyName.Direction = ParameterDirection.Input;
                    companyName.Value = (endUserDTO.CompanyName == null) ? string.Empty : endUserDTO.CompanyName;

                    SqlParameter phonenumber = cmd.Parameters.Add("@Phonenumber", SqlDbType.VarChar, 100);
                    phonenumber.Direction = ParameterDirection.Input;
                    phonenumber.Value = (endUserDTO.Phonenumber == null) ? string.Empty : endUserDTO.Phonenumber;

                    SqlParameter id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    id.Direction = ParameterDirection.Output;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthId = (int)myData["id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("WrapperDal/AddUser", "", e.Message, "Exception", 0);
            }

            return AthId;
        }


        internal static string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        internal static string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }


        internal static int CheckEndUserExistsorPassword(string emailaddress, string password)
        {

            int Id = 0;

            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[CheckEndUserExistsorPassword]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlParameter passwordPar = cmd.Parameters.Add("@password", SqlDbType.VarChar, 100);
                    passwordPar.Direction = ParameterDirection.Input;
                    passwordPar.Value = password;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        Id = (int)myData["Id"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V4/CheckCoachExistsorPassword", "", ex.Message, "Exception", 0);
                throw;
            }

            return Id;
        }


        internal static EndUserDTO GetEndUserExists(string emailaddress, string password)
        {

            EndUserDTO v = new EndUserDTO();

            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetEndUserExists]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlParameter passwordPar = cmd.Parameters.Add("@password", SqlDbType.VarChar, 100);
                    passwordPar.Direction = ParameterDirection.Input;
                    passwordPar.Value = password;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (myData["Id"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["Id"]);
                        v.Name = (myData["Name"] == DBNull.Value) ? string.Empty : myData["Name"].ToString();
                        v.Emailaddress = (myData["Emailaddress"] == DBNull.Value) ? string.Empty : myData["Emailaddress"].ToString();
                        v.Password = (myData["Password"] == DBNull.Value) ? string.Empty : myData["Password"].ToString();
                        v.CompanyName = (myData["CompanyName"] == DBNull.Value) ? string.Empty : myData["CompanyName"].ToString();
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : myData["Phonenumber"].ToString();
                        v.UserType = (myData["UserType"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["UserType"]);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V4/GetEndUserExists", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }


        internal static int PostAds(AdDTO adDTO)
        {
            int adId = 0;
            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[PostAds]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    Name.Direction = ParameterDirection.Input;
                    Name.Value = adDTO.Name;

                    SqlParameter ProfilePicURL = cmd.Parameters.Add("@ProfilePicURL", SqlDbType.NVarChar, 1000);
                    ProfilePicURL.Direction = ParameterDirection.Input;
                    ProfilePicURL.Value = adDTO.ProfilePicURL;

                    SqlParameter WebsiteURL = cmd.Parameters.Add("@WebsiteURL", SqlDbType.NVarChar, 1000);
                    WebsiteURL.Direction = ParameterDirection.Input;
                    WebsiteURL.Value = adDTO.WebsiteURL;

                    SqlParameter Description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 4000);
                    Description.Direction = ParameterDirection.Input;
                    Description.Value = (adDTO.Description == null) ? string.Empty : adDTO.Description;

                    SqlParameter AdTypePar = cmd.Parameters.Add("@AdType", SqlDbType.Int);
                    AdTypePar.Direction = ParameterDirection.Input;
                    AdTypePar.Value = adDTO.AdType;

                    SqlParameter EndAdUserIdPar = cmd.Parameters.Add("@EndAdUserId", SqlDbType.Int);
                    EndAdUserIdPar.Direction = ParameterDirection.Input;
                    EndAdUserIdPar.Value = adDTO.EndAdUserId;

                    SqlParameter IdPar = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    IdPar.Direction = ParameterDirection.InputOutput;
                    IdPar.Value = adDTO.Id;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        adId = (int)myData["Id"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V4/PostAds", "", ex.Message, "Exception", 0);
                throw;
            }

            return adId;
        }


        internal static string GetCodeForEndUser(string emailaddress)
        {
            string code = string.Empty;

            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCodeForEndUser]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        code = (string)myData["Code"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetCoachUserExists", "", ex.Message, "Exception", 0);
                throw;
            }

            return code;
        }

        internal static SupportMailDTO GetSupportMails(string name)
        {

            SupportMailDTO s = new SupportMailDTO();
            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetSupportMails]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter namePar = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 500);
                    namePar.Direction = ParameterDirection.Input;
                    namePar.Value = name;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    { 
                        s.EmailAddress = (string)myData["EmailAddress"];
                        s.Password = (string)myData["Password"];
                        s.Host = (string)myData["Host"];
                        s.Port = (string)myData["Port"];
                        s.SSL = (string)myData["SSL"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetSupportMails", "", ex.Message, "Exception", 0);
                throw;
            }

            return s;
        }


        internal static string UpdateEndUserPassword(string userName, string token, string confirmPassword)
        {

            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateEndUserPassword]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Token = cmd.Parameters.Add("@Token", SqlDbType.VarChar, 500);
                    Token.Direction = ParameterDirection.Input;
                    Token.Value = token;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = userName;

                    SqlParameter Password = cmd.Parameters.Add("@Password", SqlDbType.VarChar, 100);
                    Password.Direction = ParameterDirection.Input;
                    // Password.Value =  confirmPassword;
                    Password.Value = Encrypt(confirmPassword);

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        userName = (string)myData["Email"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/UpdateCoachPassword", "", ex.Message, "Exception", 0);
                throw;
            }

            return userName;
        }


        internal static List<AdDTO> GetAds(int endAdUserId)
        {
            int ExistingSportId = 0;
            List<AdDTO> AdDTO = new List<AdDTO>();

            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAds]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter endAdUserIdPar = cmd.Parameters.Add("@EndAdUserId", SqlDbType.Int);
                    endAdUserIdPar.Direction = ParameterDirection.Input;
                    endAdUserIdPar.Value = endAdUserId;  

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AdDTO v = new AdDTO();
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.Description = (myData["Description"] == DBNull.Value) ? "" : (string)myData["Description"].ToString();
                        v.ProfilePicURL = (myData["ProfilePicURL"] == DBNull.Value) ? "" : (string)myData["ProfilePicURL"].ToString();
                        v.WebsiteURL = (myData["WebsiteURL"] == DBNull.Value) ? "" : (string)myData["WebsiteURL"];
                        v.AdType = 4;
                        v.Id = (int)myData["Id"];
                        v.EndAdUserId = (myData["EndAdUserId"] == DBNull.Value) ? 0 : (int)myData["EndAdUserId"];
                        v.EndAdUserName = (myData["EndAdUserName"] == DBNull.Value) ? "" : (string)myData["EndAdUserName"];
                        v.EndAdUserEmail = (myData["EndAdUserEmail"] == DBNull.Value) ? "" : (string)myData["EndAdUserEmail"];
                       // v.CntOfAthletes = (myData["CntAthlete"] == DBNull.Value) ? 0 : (int)myData["CntAthlete"]; 
                        ExistingSportId = (myData["SportId"] == DBNull.Value) ? 0 : (int)myData["SportId"];  
                        if (ExistingSportId == 1000)
                        {
                            v.SportId = 1000;
                            v.SportName = "ALL Sports";
                        }
                        else
                        {
                            SportsDTO sports = GetAdSportId(v.Id);
                            v.SportId = sports.Id;
                            v.SportName = sports.Name;
                        }
                        AdDTO.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V4/GetAds", "", ex.Message, "Exception", 0);
                throw;
            }


            return AdDTO;
        }


        internal static int DeleteAd(int adId)
        {

            int Id = 0;

            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteAd]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter adIdPar = cmd.Parameters.Add("@adId", SqlDbType.Int);
                    adIdPar.Direction = ParameterDirection.Input;
                    adIdPar.Value = adId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    { 
                        Id = (int)myData["Id"]; 
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V4/DeleteAd", "", ex.Message, "Exception", 0);
                throw;
            }


            return Id;
        }

        internal static List<AdDTO> GetDisplayAdsForSuperAdmins(int endAdUserId)
        {
            int ExistingSportId = 0;

            List<AdDTO> AdDTO = new List<AdDTO>();

            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetDisplayAdsForSuperAdmins]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter endAdUserIdPar = cmd.Parameters.Add("@EndAdUserId", SqlDbType.Int);
                    endAdUserIdPar.Direction = ParameterDirection.Input;
                    endAdUserIdPar.Value = endAdUserId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AdDTO v = new AdDTO();
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.Description = (myData["Description"] == DBNull.Value) ? "" : (string)myData["Description"].ToString();
                        v.ProfilePicURL = (myData["ProfilePicURL"] == DBNull.Value) ? "" : (string)myData["ProfilePicURL"].ToString();
                        v.WebsiteURL = (myData["WebsiteURL"] == DBNull.Value) ? "" : (string)myData["WebsiteURL"];
                        v.AdType = (myData["AdType"] == DBNull.Value) ? 0 : (int)myData["AdType"]; 
                        v.Id = (int)myData["Id"];
                        v.EndAdUserId = (myData["EndAdUserId"] == DBNull.Value) ? 0 : (int)myData["EndAdUserId"];
                        v.EndAdUserName = (myData["EndAdUserName"] == DBNull.Value) ? "" : (string)myData["EndAdUserName"];
                        v.EndAdUserEmail = (myData["EndAdUserEmail"] == DBNull.Value) ? "" : (string)myData["EndAdUserEmail"];
                        ExistingSportId = (myData["SportId"] == DBNull.Value) ? 0 : (int)myData["SportId"];

                        if(ExistingSportId == 1000)
                        {
                            v.SportId = 1000;
                            v.SportName = "ALL Sports";
                        }
                        else
                        {
                            SportsDTO sports = GetAdSportId(v.Id);
                            v.SportId = sports.Id;
                            v.SportName = sports.Name;
                        }
                        AdDTO.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V4/GetDisplayAdsForSuperAdmins", "", ex.Message, "Exception", 0);
                throw;
            }


            return AdDTO;
        }

        private static SportsDTO GetAdSportId(int adid)
        {
            SportsDTO sports = new SportsDTO();
            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAdSportId]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter adidPar = cmd.Parameters.Add("@AdId", SqlDbType.Int);
                    adidPar.Direction = ParameterDirection.Input;
                    adidPar.Value = adid;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        sports.Id = (myData["SportId"] == DBNull.Value) ? 0 : (int)myData["SportId"];
                        sports.Name = (myData["sportname"] == DBNull.Value) ? string.Empty : (string)myData["sportname"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V4/GetAdSportId", "", ex.Message, "Exception", 0);
                throw;
            }


            return sports;
        }

        internal static List<AthleteandAdDTO> GetAthletesForChatAdByAdId(int AdId)
        {
            List<AthleteandAdDTO> athleteDTO = new List<AthleteandAdDTO>();

            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthletesForChatAdByAdId]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter AdIdPar = cmd.Parameters.Add("@AdId", SqlDbType.Int);
                    AdIdPar.Direction = ParameterDirection.Input;
                    AdIdPar.Value = AdId; 

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteandAdDTO v = new AthleteandAdDTO();
                        v.AdId = (myData["ChatAdId"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["ChatAdId"]);
                        v.Name = (myData["Name"] == DBNull.Value) ? string.Empty : myData["Name"].ToString();
                        v.ProfilePicURL = (myData["ProfilePicURL"] == DBNull.Value) ? string.Empty : myData["ProfilePicURL"].ToString();
                        v.WebsiteURL = (myData["WebsiteURL"] == DBNull.Value) ? string.Empty : myData["WebsiteURL"].ToString();
                        v.Description = (myData["Description"] == DBNull.Value) ? string.Empty : myData["Description"].ToString();
                        v.AdType = (myData["Adtype"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["Adtype"]);
                        v.EndAdUserId = (myData["EndAdUserId"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["EndAdUserId"]);
                        v.SportId = (myData["SportId"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["SportId"]);
                        v.Emailaddress = (myData["Emailaddress"] == DBNull.Value) ? string.Empty : myData["Emailaddress"].ToString();
                        v.Username = (myData["Username"] == DBNull.Value) ? string.Empty : myData["Username"].ToString();
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["LoginType"]);
                        v.AthleteId = (myData["AthleteID"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AthleteID"]);
                        AthleteController ath = new AthleteController();
                        string email = string.Empty;
                        if (v.LoginType == 1 || v.LoginType == 4)
                        { email = v.Emailaddress; }
                        else { email = v.Username; }
                        AthleteProfileDTO athleteProfile = ath.GetAthleteProfile(email, v.SportId, v.LoginType);
                        v.DeviceId = athleteProfile.DeviceId;
                        v.DeviceType = athleteProfile.DeviceType;
                        v.AthleteName = athleteProfile.Name;
                        v.AthleteProfilePic = athleteProfile.ProfilePicURL;

                        athleteDTO.Add(v);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v4/GetAthletesForChatAdByAdId", "", ex.Message, "Exception", 0);
                throw;
            }

            return athleteDTO;
        }

    }
}