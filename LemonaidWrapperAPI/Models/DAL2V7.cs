﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace LemonaidWrapperAPI.Models
{
    public class DAL2V7
    {
        internal static int AddCoachVideo360Content(Video360TypeDTO Video360)
        {

            int VideoId = 0;

            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddCoachVideos360Type]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter NamePar = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 50);
                    NamePar.Direction = ParameterDirection.Input;
                    NamePar.Value = Video360.Name;

                    SqlParameter videoURLPar = cmd.Parameters.Add("@VideoURL", SqlDbType.VarChar, 1024);
                    videoURLPar.Direction = ParameterDirection.Input;
                    videoURLPar.Value = Video360.VideoURL;

                    SqlParameter thumbnailURLPar = cmd.Parameters.Add("@ThumbnailURL", SqlDbType.VarChar, 1024);
                    thumbnailURLPar.Direction = ParameterDirection.Input;
                    thumbnailURLPar.Value = Video360.ThumbNailURL;

                    SqlParameter statusPar = cmd.Parameters.Add("@Status", SqlDbType.Int);
                    statusPar.Direction = ParameterDirection.Input;
                    statusPar.Value = Video360.Status;

                    SqlParameter coachIdPar = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    coachIdPar.Direction = ParameterDirection.Input;
                    coachIdPar.Value = Video360.CoachId;

                    SqlParameter videoformatPar = cmd.Parameters.Add("@VideoFormat", SqlDbType.VarChar, 50);
                    videoformatPar.Direction = ParameterDirection.Input;
                    videoformatPar.Value = Video360.VideoFormat;

                    SqlParameter durationPar = cmd.Parameters.Add("@duration", SqlDbType.Int);
                    durationPar.Direction = ParameterDirection.Input;
                    durationPar.Value = Video360.Duration;

                    SqlParameter latitudePar = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 100);
                    latitudePar.Direction = ParameterDirection.Input;
                    latitudePar.Value = Video360.Latitude;

                    SqlParameter longitudePar = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 100);
                    longitudePar.Direction = ParameterDirection.Input;
                    longitudePar.Value = Video360.Longitude;

                    SqlParameter Descpar = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 500);
                    Descpar.Direction = ParameterDirection.Input;
                    Descpar.Value = Video360.Description;

                    SqlParameter Authorpar = cmd.Parameters.Add("@Author", SqlDbType.VarChar, 1000);
                    Authorpar.Direction = ParameterDirection.Input;
                    Authorpar.Value = Video360.Author; 

                    SqlParameter EmailaddressPar = cmd.Parameters.Add("@CoachEmailaddress", SqlDbType.VarChar, 100);
                    EmailaddressPar.Direction = ParameterDirection.Input;
                    EmailaddressPar.Value = Video360.Emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideoId = (int)myData["Video360ID"];
                    }

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V7/AddCoachVideo360Content", "", ex.Message, "Exception", 0);
            }


            return VideoId;
        }


        internal static List<Video360TypeDTO> GetCoachVideos360Type()
        {
            List<Video360TypeDTO> lstVideo360Type = new List<Video360TypeDTO>();

            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCoachVideos360Type]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        Video360TypeDTO v = new Video360TypeDTO();
                        v.Name = myData["Name"].ToString();
                        v.VideoURL = myData["VideoURL"].ToString();
                        v.ThumbNailURL = myData["ThumbnailURL"].ToString();
                        v.CoachId = Convert.ToInt16(myData["CoachId"]);
                        v.VideoFormat = myData["VideoFormat"].ToString();
                        v.Duration = (myData["Duration"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["Duration"]);
                        v.VideoType = (myData["VideoType"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["VideoType"]);
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.Description = myData["Description"].ToString();
                        v.Author = myData["Author"].ToString();
                        lstVideo360Type.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v7/GetCoachVideos360Type", "", ex.Message, "Exception", 0);
            }

            return lstVideo360Type;
        }

        internal static int DeleteCoachVideo360Type(int Video360ID)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteCoachVideo360Type]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter VideoID = cmd.Parameters.Add("@VideoID", SqlDbType.Int);
                    VideoID.Direction = ParameterDirection.Input;
                    VideoID.Value = Video360ID;



                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V7/DeleteCoachVideo360Type", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static List<Video360TypeDTO> GetCoachVideos360Type(string emailaddress)
        {
            List<Video360TypeDTO> lstVideo360Type = new List<Video360TypeDTO>();

            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCoachVideos360TypeByCoach]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        Video360TypeDTO v = new Video360TypeDTO();
                        v.Id = Convert.ToInt16(myData["Id"]);
                        v.Name = myData["Name"].ToString();
                        v.VideoURL = myData["VideoURL"].ToString();
                        v.ThumbNailURL = myData["ThumbnailURL"].ToString();
                        v.CoachId = Convert.ToInt16(myData["CoachId"]);
                        v.VideoFormat = myData["VideoFormat"].ToString();
                        v.Duration = (myData["Duration"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["Duration"]);
                        v.VideoType = (myData["VideoType"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["VideoType"]);
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.Description = myData["Description"].ToString();
                        v.Author = myData["Author"].ToString();
                        lstVideo360Type.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v7/GetCoachVideos360Type", "", ex.Message, "Exception", 0);
            }

            return lstVideo360Type;
        }

        internal static List<Video360TypeDTO> GetCoachVideos360TypeBySearchText(CoachVideo360 coachVideo360)
        {
            List<Video360TypeDTO> lstVideo360Type = new List<Video360TypeDTO>();

            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCoachVideos360TypeBySearchtext]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Text = cmd.Parameters.Add("@SearchText", SqlDbType.VarChar, 100);
                    Text.Direction = ParameterDirection.Input;
                    Text.Value = coachVideo360.SearchText;

                    SqlParameter Offset = cmd.Parameters.Add("@Offset", SqlDbType.Int);
                    Offset.Direction = ParameterDirection.Input;
                    Offset.Value = coachVideo360.Offset;

                    SqlParameter Max = cmd.Parameters.Add("@Max", SqlDbType.Int);
                    Max.Direction = ParameterDirection.Input;
                    Max.Value = coachVideo360.Max;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        Video360TypeDTO v = new Video360TypeDTO();
                        v.Name = myData["Name"].ToString();
                        v.VideoURL = myData["VideoURL"].ToString();
                        v.ThumbNailURL = myData["ThumbnailURL"].ToString();
                        v.CoachId = Convert.ToInt16(myData["CoachId"]);
                        v.VideoFormat = myData["VideoFormat"].ToString();
                        v.Duration = (myData["Duration"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["Duration"]);
                        v.VideoType = (myData["VideoType"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["VideoType"]);
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.Description = myData["Description"].ToString();
                        v.Author = myData["Author"].ToString();
                        lstVideo360Type.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v7/GetCoachVideos360TypeBySearchText", "", ex.Message, "Exception", 0);
            }

            return lstVideo360Type;
        }

        internal static int UpdateCoach360VideoName(Video360TypeDTO video360Type)
        {
            int id = 0;

            try
            {
                SqlConnection myConn = DAL.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateCoach360VideoName]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter NamePar = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 50);
                    NamePar.Direction = ParameterDirection.Input;
                    NamePar.Value = video360Type.Name;

                    SqlParameter Coach360IdPar = cmd.Parameters.Add("@Coach360Id", SqlDbType.VarChar, 100);
                    Coach360IdPar.Direction = ParameterDirection.Input;
                    Coach360IdPar.Value = video360Type.Id; 

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        id = (int)myData["Id"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/UpdateCoachPassword", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

    }
}