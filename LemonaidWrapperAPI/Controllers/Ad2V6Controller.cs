﻿using System; 
using LemonaidWrapperAPI.Models;
using System.Web.Http;

namespace LemonaidWrapperAPI.Controllers
{
    public class Ad2V6Controller : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();
        [HttpPost]
        public Utility.CustomResponse AddUserAdMatch(AthleteAdDTO athleteAdDTO)
        {
            try
            {

                var matchId = DAL2V6.AddAthleteChatAdMatch(athleteAdDTO);
                _result.Status = Utility.CustomResponseStatus.Successful;
                matchId = (athleteAdDTO.Status == false) ? 0 : matchId;
                _result.Response = matchId;
                _result.Message = CustomConstants.Status_Added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Ad2V6/AddUserAdMatch", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;

            }
        }


        [HttpPost]
        public Utility.CustomResponse AddUserAdChat(ChatDTO chatDto)
        {
            try
            {

                int UserId = DAL2V6.GetUserId(chatDto);

                if (chatDto.ReceiverType == 4)
                {
                    chatDto.SenderDBId = UserId;
                    chatDto.ReceiverDBId = chatDto.AdId;
                }
                else if (chatDto.SenderType == 4)
                {
                    chatDto.ReceiverDBId = UserId;
                    chatDto.SenderDBId = chatDto.AdId;
                }

                var chatid = DAL2V4.AddAthleteAdChat(chatDto);

                if (chatid > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = chatid;
                    _result.Message = CustomConstants.Chat_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }



                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse DeleteAdUserMatch(AthleteAdDTO athleteAdDTO)
        {

            try
            {

                var matchId = DAL2V6.DeleteChatAdAtheleteMatch(athleteAdDTO);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = matchId;
                _result.Message = CustomConstants.Match_Removed;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Ad/DeleteAdUserMatch", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse GetAthleteAdChatMessages(ChatDTO chatDto)
        {
            try
            {
                int senderFromUser = chatDto.SenderId;
                int receFromUser = chatDto.ReceiverId;

                int UserId = DAL2V6.GetUserId(chatDto);

                if (chatDto.ReceiverType == 4)
                {
                    chatDto.SenderDBId = UserId;
                    chatDto.ReceiverDBId = chatDto.AdId;
                }
                else if (chatDto.SenderType == 4)
                {
                    chatDto.ReceiverDBId = UserId;
                    chatDto.SenderDBId = chatDto.AdId;
                }

                var chats = DAL2V4.GetAthleteChatAdChatMessages(chatDto);

                foreach (var i in chats)
                {
                    if (i.SenderType != 4)
                    {
                        i.SenderId = senderFromUser;
                    }
                    if (i.ReceiverType != 4)
                    {
                        i.ReceiverId = senderFromUser;
                    }


                }
                if (chats.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = chats;
                    _result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }



                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Ad/GetAthleteAdChatMessages", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetUsersForAdByAdId(int AdId,string UserType)
        {

            try
            {
                var athletes = DAL2V6.GetUsersForChatAdByAdId(AdId, UserType);
                foreach (var a in athletes)
                {
                    a.ChatCount = DAL2V4.GetAthleteChatAdChatMessagesUnreadCount(a.AthleteId, a.AdId);
                }

                if (athletes.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = athletes;
                    _result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Ad2V6/GetUsersForAdByAdId", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

    }
}
