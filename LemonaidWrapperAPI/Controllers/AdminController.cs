﻿using System;
using LemonaidWrapperAPI.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using SendGrid;

namespace LemonaidWrapperAPI.Controllers
{
    public class AdminController : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse AddEndUser(EndUserDTO endUserDTO)
        { 
            try
            { 
                var usrId = DAL2V4.AddEndUser(endUserDTO);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = usrId;
                _result.Message = CustomConstants.UserAdded;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpGet]
        public Utility.CustomResponse Login(string emailaddress, string password)
        {
            try
            {
                int id = DAL2V4.CheckEndUserExistsorPassword(emailaddress, DAL2V4.Encrypt(password));
                if (id == -1)
                {
                    _result.Status = Utility.CustomResponseStatus.UserDoesNotExist;
                    _result.Message = CustomConstants.EndAd_NewUser;
                }
                else if (id == -2)
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.User_Exists_Already;
                }
                else
                {
                    EndUserDTO endUserDTO = DAL2V4.GetEndUserExists(emailaddress, DAL2V4.Encrypt(password));
                    if (endUserDTO.Id > 0)
                    {
                        _result.Response = endUserDTO;
                        _result.Status = Utility.CustomResponseStatus.Successful;
                        _result.Message = CustomConstants.Login_successfully;
                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Response = endUserDTO;
                        _result.Message = CustomConstants.User_Does_Not_Exist;
                    }
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Admin/Login", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }



        [HttpGet]
        public Utility.CustomResponse ForgotPassword(string emailaddress)
        {

            Utility.CustomResponse res = new Utility.CustomResponse();

            try
            {

                string code = DAL2V4.GetCodeForEndUser(emailaddress);
                if (code != string.Empty)
                {
                    string token = code; 

                    if (SendMailForForgotPassword(emailaddress, token) == 0)
                    {
                        res.Status = Utility.CustomResponseStatus.Successful;
                        res.Message = CustomConstants.ForgotPassword_successfully;
                    }
                    else
                    {
                        res.Status = Utility.CustomResponseStatus.UnSuccessful;
                        res.Message = string.Empty;  
                    }
                }
                else
                {
                    res.Status = Utility.CustomResponseStatus.UnSuccessful;
                    res.Message = CustomConstants.User_Does_Not_Exist;
                }
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result; 
            }

            return res;
        }

        [HttpGet]
        public Utility.CustomResponse ResetPassword(string userName, string token, string confirmPassword)
        {

            try
            {  
                string email = DAL2V4.UpdateEndUserPassword(userName, token, confirmPassword);
                if (email != string.Empty)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Message = CustomConstants.ResetPassword;

                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.ResetPassword_Failed;
                }


            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result; 
            }
            return _result;
        }

        public static int SendMailForForgotPassword(string username, string token)
        {
            SupportMailDTO support = new SupportMailDTO();
            MailMessage msg = new MailMessage();
            string apiUrl = string.Empty;
            try
            {

                support = DAL2V4.GetSupportMails("RequiredEmail");

                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/ForgotPassword.html"));
                string readFile = reader.ReadToEnd();
                var myString = readFile;
                myString = myString.Replace("$$Name$$", username);
                myString = myString.Replace("$$Token$$", token);
                string baseURL = ConfigurationManager.AppSettings["BaseURL"];
                myString = myString.Replace("$$BaseURL$$", baseURL);

                SendGridMailModel sendGridModel = new SendGridMailModel();
                sendGridModel.toMail = new List<string>();
                sendGridModel.toMail.Add(username);
                sendGridModel.fromMail = support.EmailAddress;
                sendGridModel.fromName = "Lemonaid";
                sendGridModel.subject = " Forgot Password";
                sendGridModel.Message = myString;
                SendGridSMTPMail.SendEmail(sendGridModel);

                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog(support.EmailAddress, username, msg.Subject, "Mail", 1);
                return 0;
            }
            catch (SmtpException exx)
            {
             
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog(support.EmailAddress, username, msg.Subject, "Mail", 0);
                return 1;
            }
            catch (Exception ex)
            {
                
                return 1;
            }
        }




    }
}
