﻿using LemonaidWrapperAPI.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;

namespace LemonaidWrapperAPI.Controllers
{
    public class Coach2V6Controller : ApiController
    { 
        [HttpGet]
        public Utility.CustomResponse CoachLogin(string emailaddress, string password)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();

            try
            {
                AthleteController athl = new AthleteController();
                List<SportsDTO> sports;
                sports = DAL.GetSportsFromEmail(emailaddress, 0, UserTypes.CollegeCoach);
                if (sports.Count > 0)
                {
                    URL urls = new URL();
                    urls = DAL.GetURLsForSports(Convert.ToInt32(sports[0].Id));
                    string baseUrl = string.Empty;

                    var envirmt = ConfigurationManager.AppSettings["Environment"].ToString();
                    if (envirmt == "1")
                        baseUrl = urls.Test;
                    else if (envirmt == "2")
                        baseUrl = urls.Staging;
                    else
                        baseUrl = urls.Production;

                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri(baseUrl);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.GetAsync("/Duologin/Login?emailaddress=" + emailaddress + "&password=" + password).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                        if (res.Status == Utility.CustomResponseStatus.Successful)
                        {
                            CoachDTO coach = athl.JsonDeserialize<CoachDTO>(res.Response.ToString());
                            if (coach != null)
                            {
                                coach.SportId = sports[0].Id;
                                coach.SportName = sports[0].Name;
                            }
                            res.Response = coach; 
                        }
                        else
                        {
                            res.Response = null;
                            res.Status = Utility.CustomResponseStatus.UnSuccessful;
                            res.Message = CustomConstants.Password_Mismatch; 
                        }
                    }

                }
                else
                {
                    res.Response = null;
                    res.Status = Utility.CustomResponseStatus.UnSuccessful;
                    res.Message = CustomConstants.User_Does_Not_Exist;
                }


            }
            catch (Exception ex)
            {
                res.Status = Utility.CustomResponseStatus.Exception;
                res.Message = ex.Message;
                return res;// javaScriptSerializer.Serialize(_result);
            }

            return res;
        } 

    }
}
