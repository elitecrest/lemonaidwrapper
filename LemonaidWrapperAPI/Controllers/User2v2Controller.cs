﻿using LemonaidWrapperAPI.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Net.Http;
using System.Net.Http.Headers;

namespace LemonaidWrapperAPI.Controllers
{
    public class User2v2Controller : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        public object GetClubCoachProfile(string emailaddress, int sportId, int logintype)
        {
            AthleteController athl = new AthleteController();
            string url = athl.GetURL(sportId);
            ClubCoachDTO clubcoach = new ClubCoachDTO();

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = client.GetAsync("ClubCoach2V2/ClubCoachProfile?emailaddress=" + emailaddress + "&logintype=" + logintype).Result;
            if (response.IsSuccessStatusCode)
            {
                Utility.CustomResponse res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                clubcoach = athl.JsonDeserialize<ClubCoachDTO>(res.Response.ToString());

            }

            List<SportsDTO> sports;
            sports = DAL.GetSportsFromEmail(emailaddress, logintype, UserTypes.ClubCoach);
            clubcoach.ClubCoachSports = sports;

            return clubcoach;
        }

        public object GetFamilyFriendsProfile(string emailaddress, int sportId, int logintype)
        {
            AthleteController athl = new AthleteController();
            string url = athl.GetURL(sportId);
            FamilyFriendsDTO familyFriends = new FamilyFriendsDTO();

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = client.GetAsync("FamilyFriends2V2/GetFamilyFriendsProfile?emailaddress=" + emailaddress + "&logintype=" + logintype).Result;
            if (response.IsSuccessStatusCode)
            {
                Utility.CustomResponse res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                familyFriends = athl.JsonDeserialize<FamilyFriendsDTO>(res.Response.ToString());

            }

            List<SportsDTO> sports;
            sports = DAL.GetSportsFromEmail(emailaddress, logintype , UserTypes.FamilyFriends);
            familyFriends.FamilyFriendsSports = sports;

            return familyFriends;
        }


        private object GetProfileFromUserType(string emailaddress, int sportId, int logintype, string userType)
        {
            object profile = null;
            UserController usrCtrl = new UserController();

            if (userType == UserTypes.HighSchooAthlete || userType == UserTypes.CollegeAthlete)
            {
                profile = new AthleteProfileDTO();
                AthleteController athl = new AthleteController();
                profile = athl.GetAthleteProfile(emailaddress, sportId, logintype);
            }
            else if (userType == UserTypes.CollegeCoach)
            {
                profile = new CoachDTO();
                profile = usrCtrl.GetCoachProfile(emailaddress, sportId);
            }
            else if (userType == UserTypes.ClubCoach)
            {
                profile = new ClubCoachDTO();
                profile = GetClubCoachProfile(emailaddress, sportId, logintype);

            }
            else if (userType == UserTypes.FamilyFriends)
            {
                profile = new FamilyFriendsDTO();
                profile = GetFamilyFriendsProfile(emailaddress, sportId, logintype);

            }

            return profile;
        }

        [HttpGet]
        public Utility.CustomResponse CheckUser(string emailaddress, int logintype, string userType)
        {
            try
            {
                //bool isexists = false;
                int sportId = 0;
                List<SportsDTO> userSports = DAL.GetUserSports(emailaddress, logintype, userType);
                foreach (var i in userSports)
                {
                    if (i.Active == 1)
                        sportId = i.Id;
                }
                int cnt = userSports.Count;
                if (cnt == 0)
                {
                    if (userType == UserTypes.CollegeCoach)
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Response = 0;
                        _result.Message = CustomConstants.DetailsGetSuccessfully;
                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.NewUser;
                        _result.Response = 0;
                        _result.Message = CustomConstants.DetailsGetSuccessfully;
                    }
                }
                else if (cnt > 3 && userType != UserTypes.FamilyFriends)
                {

                    _result.Status = Utility.CustomResponseStatus.UserNotAllowed;
                    _result.Response = 0;
                    _result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    if (userType == UserTypes.HighSchooAthlete)
                    {
                        AthleteProfileDTO athlete = new AthleteProfileDTO();
                        AthleteProfileDTO athleteProfile = null;
                        //Get Ids From Profile
                        foreach (var i in userSports)
                        {
                            athlete =
                                (AthleteProfileDTO)GetProfileFromUserType(emailaddress, i.Id, logintype, userType);
                            i.AthleteId = athlete.Id;

                            if (i.Active == 1)
                            {
                                athleteProfile = athlete;
                                athleteProfile.AthleteSports = userSports;

                            }
                        }
                        if (athleteProfile.Id > 0)
                        {
                            _result.Status = Utility.CustomResponseStatus.Successful;
                            _result.Response = athleteProfile;
                            _result.Message = CustomConstants.DetailsGetSuccessfully;
                        }
                        else
                        {
                            var j = DAL.DeleteUserFromWrapper(emailaddress, sportId, logintype, userType);
                            _result.Status = Utility.CustomResponseStatus.NewUser;
                            _result.Response = 0;
                            _result.Message = CustomConstants.DetailsGetSuccessfully;

                        }
                    }
                    else
                    {
                        var profile = GetProfileFromUserType(emailaddress, sportId, logintype, userType);
                        if (profile != null)
                        {
                            _result.Status = Utility.CustomResponseStatus.Successful;
                            _result.Response = profile;
                            _result.Message = CustomConstants.DetailsGetSuccessfully;
                        }
                        else
                        {
                            var i = DAL.DeleteUserFromWrapper(emailaddress, sportId, logintype, userType);
                            _result.Status = Utility.CustomResponseStatus.NewUser;
                            _result.Response = 0;
                            _result.Message = CustomConstants.DetailsGetSuccessfully;

                        }
                    }


                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }


        }

    }
}
