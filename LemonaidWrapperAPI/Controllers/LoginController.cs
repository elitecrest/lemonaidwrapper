﻿using System; 
  
using System.Web.Http; 
 
using LemonaidWrapperAPI.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;

namespace LemonaidWrapperAPI.Controllers
{
    public class LoginController : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();


        //[HttpGet]
        //public Utility.CustomResponse GetApiUrl(int sportId,int profileId)
        //{
        //    string url = string.Empty;
        //    try
        //    {
        //        if (sportId == 1)
        //        {
        //            if (profileId == 1)
        //            {
        //                url = "http://lemonaidbizapi.azurewebsites.net";
        //            }
        //            else if (profileId == 2)
        //            {
        //                url = "http://lemonaidbizapitest.azurewebsites.net";
        //            }
        //            else if (profileId == 3)
        //            {
        //                url = "http://lemonaidstagingapi.azurewebsites.net";
        //            }
        //        }
        //        else if (sportId == 2)
        //        {
        //            if (profileId == 1)
        //            {
        //                url = "http://lemonaidrowingapi.azurewebsites.net";
        //            }
        //            else if (profileId == 2)
        //            {
        //                url = "http://lemonaidrowingapitest.azurewebsites.net";
        //            }
        //            else if (profileId == 3)
        //            {
        //                url = "http://lemonaidrowingapistaging.azurewebsites.net";
        //            }
        //        }
        //        else if (sportId == 3)
        //        {
        //            if (profileId == 1)
        //            {
        //                url = "http://lemonaidtennisapi.azurewebsites.net";
        //            }
        //            else if (profileId == 2)
        //            {
        //                url = "http://lemonaidtennisapitest.azurewebsites.net";
        //            }
        //            else if (profileId == 3)
        //            {
        //                url = "http://lemonaidtennisapistaging.azurewebsites.net";
        //            }
        //        }
        //        else if (sportId == 4)
        //        {
        //            if (profileId == 1)
        //            {
        //                url = "http://lemonaidcrossapi.azurewebsites.net";
        //            }
        //            else if (profileId == 2)
        //            {
        //                url = "http://lemonaidcrossapitest.azurewebsites.net";
        //            }
        //            else if (profileId == 3)
        //            {
        //                url = "http://lemonaidcrossapistaging.azurewebsites.net";
        //            }
        //        }
        //        if (url != string.Empty)
        //        {
        //            _result.Status = Utility.CustomResponseStatus.Successful;
        //            _result.Response = url;
        //            _result.Message = CustomConstants.DetailsGetSuccessfully;
        //        }
        //        else
        //        {
        //            _result.Status = Utility.CustomResponseStatus.UnSuccessful;
        //            _result.Message = CustomConstants.NoRecordsFound;
        //        }

        //        return _result;
        //    }
        //    catch (Exception ex)
        //    {
        //        _result.Status = Utility.CustomResponseStatus.Exception;
        //        _result.Message = ex.Message;
        //        return _result;
        //    }

        //}


        [HttpGet]
        public Utility.CustomResponse GetSports()
        { 

            try
            {

               var sports = DAL.GetSports();

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = sports;
                _result.Message = CustomConstants.DetailsGetSuccessfully;


                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpGet]
        public Utility.CustomResponse GetSportsFromEmail(string emailaddress, int loginType,string userType)
        {

            try
            {

                var sports = DAL.GetSportsFromEmail(emailaddress, loginType, userType); 

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = sports;
                _result.Message = CustomConstants.DetailsGetSuccessfully;


                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse ForgotPassword(string userName)
        {

            Utility.CustomResponse res = new Utility.CustomResponse();

            try
            {
                List<SportsDTO> sports;
                sports = DAL.GetSportsFromEmail(userName, 0, UserTypes.CollegeCoach);
                if(sports.Count > 0)
                {
                    //Get the sport URL and go to that sport Forgot password
                    URL urls = new URL();
                    urls = DAL.GetURLsForSports(Convert.ToInt32(sports[0].Id));
                    string baseUrl = string.Empty;

                    var envirmt = ConfigurationManager.AppSettings["Environment"].ToString();
                    if (envirmt == "1")
                        baseUrl = urls.Test;
                    else if (envirmt == "2")
                        baseUrl = urls.Staging;
                    else
                        baseUrl = urls.Production; 

                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri(baseUrl);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.GetAsync("/Coach/ForgotPassword?userName=" + userName).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                        if (res.Status == Utility.CustomResponseStatus.Successful)
                        {
                            res.Status = Utility.CustomResponseStatus.Successful;
                            res.Message = CustomConstants.ForgotPassword_successfully;
                        }
                        else
                        {
                            res.Status = Utility.CustomResponseStatus.UnSuccessful;
                            res.Message = CustomConstants.User_Does_Not_Exist;   
                          
                        }
 
                    }
                }
                else
                {
                    res.Status = Utility.CustomResponseStatus.UnSuccessful;
                    res.Message = CustomConstants.User_Does_Not_Exist;
                }
               
            }
            catch (Exception ex)
            {
                res.Status = Utility.CustomResponseStatus.Exception;
                res.Message = ex.Message;
                return _result;// javaScriptSerializer.Serialize(_result);
            }

            return res;
        }


        [HttpGet]
        public Utility.CustomResponse GetAllSports()
        {

            try
            {

                var sports = DAL.GetAllSports();

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = sports;
                _result.Message = CustomConstants.DetailsGetSuccessfully;


                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


    }
}
