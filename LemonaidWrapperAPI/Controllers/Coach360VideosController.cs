﻿using LemonaidWrapperAPI.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Net.Http;
using System.Net.Http.Headers;

namespace LemonaidWrapperAPI.Controllers
{
    public class Coach360VideosController : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse AddCoach360Videos(Video360TypeDTO Video360)
        {
            try
            {
                int id = DAL2V7.AddCoachVideo360Content(Video360);
                _result.Response = id;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.Video360_Added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach360Videos/AddCoach360Videos", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpGet]
        public Utility.CustomResponse DeleteCoach360Videos(int Video360ID)
        {
            try
            {
                var DeleteID = DAL2V7.DeleteCoachVideo360Type(Video360ID);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = Video360ID;
                _result.Message = CustomConstants.Video_Deleted_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach360Videos/DeleteCoach360Videos", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetCoachVideos360Type()
        {

            try
            {
                List<Video360TypeDTO> lstVideo360Type = DAL2V7.GetCoachVideos360Type();

                if (lstVideo360Type.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = lstVideo360Type;
                    _result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach360Videos/GetCoachVideos360Type", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse GetCoachVideos360TypeBySearchText(CoachVideo360 coachVideo360)
        {

            try
            {
                List<Video360TypeDTO> lstVideo360Type = DAL2V7.GetCoachVideos360TypeBySearchText(coachVideo360);

                if (lstVideo360Type.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = lstVideo360Type;
                    _result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach360Videos/GetCoachVideos360TypeBySearchText", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetCoachVideos360Type(string emailaddress)
        {

            try
            {
                List<Video360TypeDTO> lstVideo360Type = DAL2V7.GetCoachVideos360Type(emailaddress);

                if (lstVideo360Type.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = lstVideo360Type;
                    _result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach360Videos/GetCoachVideos360Type", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse UpdateCoach360VideoName(Video360TypeDTO video360Type)
        {

            try
            {
                int id = DAL2V7.UpdateCoach360VideoName(video360Type);

                if (id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = id;
                    _result.Message = CustomConstants.VideoName_Updated_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach360Videos/UpdateCoach360VideoName", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }
    }

}
