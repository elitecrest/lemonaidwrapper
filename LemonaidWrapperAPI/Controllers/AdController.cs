﻿using System;
using LemonaidWrapperAPI.Models;
using System.Web.Http;
using System.Collections.Generic;

namespace LemonaidWrapperAPI.Controllers
{
    public class AdController : ApiController
    {

        Utility.CustomResponse _result = new Utility.CustomResponse();
        [HttpPost]
        public Utility.CustomResponse AddAthleteAdMatch(AthleteAdDTO athleteAdDTO)
        {
            try
            {

                var matchId = DAL2V4.AddAthleteChatAdMatch(athleteAdDTO);
                _result.Status = Utility.CustomResponseStatus.Successful;
                matchId = (athleteAdDTO.Status == false) ? 0 : matchId;
                _result.Response = matchId;
                _result.Message = CustomConstants.Status_Added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Ad/AddAthleteAdMatch", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;

            }
        }


        [HttpPost]
        public Utility.CustomResponse AddAthleteAdChat(ChatDTO chatDto)
        {
            try
            {

                int UserId  = DAL2V4.GetUserId(chatDto);

                if(chatDto.SenderType == 0 && chatDto.ReceiverType == 1)
                {
                    chatDto.SenderDBId = UserId;
                    chatDto.ReceiverDBId = chatDto.AdId;
                }
                else if (chatDto.SenderType == 1 && chatDto.ReceiverType == 0)
                {
                    chatDto.ReceiverDBId = UserId;
                    chatDto.SenderDBId = chatDto.AdId;
                }
                
                var chatid = DAL2V4.AddAthleteAdChat(chatDto);
              
                if (chatid > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = chatid;
                    _result.Message = CustomConstants.Chat_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }



                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse DeleteAdAtheleteMatch(AthleteAdDTO athleteAdDTO)
        {

            try
            {

                var matchId = DAL2V4.DeleteChatAdAtheleteMatch(athleteAdDTO);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = matchId;
                _result.Message = CustomConstants.Match_Removed;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Ad/DeleteAdAtheleteMatch", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse GetAthleteAdChatMessages(ChatDTO chatDto)
        {
            try
            {
                int senderFromUser = chatDto.SenderId;
                int receFromUser = chatDto.ReceiverId;

                int UserId = DAL2V4.GetUserId(chatDto);

                if (chatDto.SenderType == 0 && chatDto.ReceiverType == 1)
                {
                    chatDto.SenderDBId = UserId;
                    chatDto.ReceiverDBId = chatDto.AdId;
                }
                else if (chatDto.SenderType == 1 && chatDto.ReceiverType == 0)
                {
                    chatDto.ReceiverDBId = UserId;
                    chatDto.SenderDBId = chatDto.AdId;
                }

                var chats = DAL2V4.GetAthleteChatAdChatMessages(chatDto);

                foreach (var i in chats)
                {
                    if (i.SenderType == 0)
                    {
                        i.SenderId = senderFromUser;
                    }
                    if (i.ReceiverType == 0)
                    {
                        i.ReceiverId = senderFromUser;
                    }


                }
                if (chats.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = chats;
                    _result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }



                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Ad/GetAthleteAdChatMessages", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetAthletesForAd(int EndAdUserId, int AdType)
        {

            try
            {
                var athletes = DAL2V4.GetAthletesForChatAd(EndAdUserId, AdType);
                foreach (var a in athletes)
                {
                    a.ChatCount = DAL2V4.GetAthleteChatAdChatMessagesUnreadCount(a.AthleteId, a.AdId);
                }

                if (athletes.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = athletes;
                    _result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Ad/GetAthletesForAd", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpGet]
        public Utility.CustomResponse GetAthletesForAdByAdId(int AdId)
        {

            try
            {
                var athletes = DAL2V4.GetAthletesForChatAdByAdId(AdId);
                foreach (var a in athletes)
                {
                    a.ChatCount = DAL2V4.GetAthleteChatAdChatMessagesUnreadCount(a.AthleteId, a.AdId);
                }

                if (athletes.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = athletes;
                    _result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Ad/GetAthletesForAdByAdId", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

      

        [HttpGet]
        public Utility.CustomResponse GetEndUserId(string Name)
        { 
            try
            {
                var ad = DAL2V4.GetEndUserId(Name); 

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = ad;
                _result.Message = CustomConstants.DetailsGetSuccessfully; 

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Ad/GetEndUserId", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpPost]
        public Utility.CustomResponse PostAds(AdDTO adDTO)
        {
            List<int> sportids = new List<int>() {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
             
            try
            {
                //Adtype 1 - WebsiteAd, 4 - Chat Ad
                var Id = DAL2V4.PostAds(adDTO);
                if(adDTO.SportId == 1000) //ALL Sports
                {
                    foreach(var i in sportids)
                    {
                        adDTO.Id = Id;
                        adDTO.SportId = i;
                        var chatSportId = DAL2V6.AddChatAdSports(adDTO);
                    }
                }
                else
                {
                    adDTO.Id = Id;                    
                    var id = DAL2V6.AddChatAdSports(adDTO);
                }
                if (Id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = Id;
                    _result.Message = CustomConstants.Ad_Uploaded;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Ad/PostAds", "", ex.Message, "Exception", 0);
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse GetAds(int endAdUserId)
        {
            AdChatCountDTO adChatCountDTO = new AdChatCountDTO();
            try
            {

                var ads = DAL2V4.GetAds(endAdUserId);
                foreach(var a in ads)
                {
                    adChatCountDTO = DAL2V6.GetChatCountByChatID(a.Id);
                    a.Count = adChatCountDTO;
                }
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = ads;
                _result.Message = CustomConstants.DetailsGetSuccessfully;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Ad/GetAds", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            } 

        }


        [HttpGet]
        public Utility.CustomResponse DeleteAd(int adId)
        {
            try
            {
                var id = DAL2V4.DeleteAd(adId);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = id;
                _result.Message = CustomConstants.DetailsGetSuccessfully;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Ad/DeleteAd", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }



        [HttpGet]
        public Utility.CustomResponse GetDisplayAdsForSuperAdmins(int endAdUserId)
        {
            try
            {
                var ads = DAL2V4.GetDisplayAdsForSuperAdmins(endAdUserId);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = ads;
                _result.Message = CustomConstants.DetailsGetSuccessfully;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Ad/GetDisplayAdsForSuperAdmins", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }
    }
}
