﻿using LemonaidWrapperAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Http;

namespace LemonaidWrapperAPI.Controllers
{
    public class AthleteController : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse AddAthlete(AthleteDTO athleteDto)
        {

            try
            { 
                var athleteId  = DAL.AddAthlete(athleteDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = athleteId;
                _result.Message = CustomConstants.AthleteAdded;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        public AthleteProfileDTO GetAthleteProfile(string emailaddress, int sportId, int logintype)
        {

            string url =  GetURL(sportId);
             AthleteProfileDTO athlete = new AthleteProfileDTO();

            HttpClient client = new HttpClient(); 
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = client.GetAsync("/Athlete2v4/AtheleteProfile?emailaddress=" + emailaddress + "&logintype=" + logintype + "&sportid=" + sportId).Result;
            if (response.IsSuccessStatusCode)
            {
                 Utility.CustomResponse res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                athlete  = JsonDeserialize<AthleteProfileDTO>(res.Response.ToString());

            }

            //List<SportsDTO> sports = new List<SportsDTO>();
            //sports = DAL.GetSportsFromEmail(emailaddress, logintype, UserTypes.HighSchooAthlete);
            //athlete.AthleteSports = sports;
            //foreach (SportsDTO t in athlete.AthleteSports)
            //{
            //    if (t.Id == sportId)
            //    {
            //        t.Active = 1;

            //    }
            //}
            return athlete;
        }



        public T JsonDeserialize<T>(string jsonString)
        { 
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        public string GetURL(int sportId)
        {
            
            URL urls = new URL();
            urls = DAL.GetURLsForSports(sportId);
            string baseUrl = string.Empty;

            var envirmt = ConfigurationManager.AppSettings["Environment"].ToString();
            if (envirmt == "1")
                baseUrl = urls.Test;
            else if (envirmt == "2")
                baseUrl = urls.Staging;
            else
                baseUrl = urls.Production;


            return baseUrl;
        }


        [HttpPost]
        public Utility.CustomResponse AddAthleteSport(AthleteSportsDTO athleteSportsDto)
        {
            try
            {
                var athlete = DAL.AddAthleteSports(athleteSportsDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = athlete;
                _result.Message = CustomConstants.AthleteSportAdded;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse CheckAthlete(string emailaddress, int sportId,int logintype)
        {

            try
            {
                bool isexists = false;
                AthleteProfileDTO athleteProfile = new AthleteProfileDTO();
                //int i = DAL.CheckAthlete(emailaddress, sportId);
                List<AthleteSportsDTO> athleteSports = DAL.GetAthleteSports(emailaddress, logintype);
                int cnt = athleteSports.Count;
                if (cnt == 0)
                {
                    
                    _result.Status = Utility.CustomResponseStatus.NewUser;
                    _result.Response = 0;
                    _result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else if (cnt > 3)
                { 
                    
                    _result.Status = Utility.CustomResponseStatus.UserNotAllowed;
                    _result.Response = 0;
                    _result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else  
                {
                    for (int i = 0; i < cnt; i++)
                    {
                        if (sportId == athleteSports[i].SportId)
                        {
                            isexists = true;
                        }
                    }
                    if (isexists)
                    {
                        //Get Athlete profile from that sport
                       athleteProfile = GetAthleteProfile(emailaddress, sportId, logintype);
                       _result.Status = Utility.CustomResponseStatus.Successful;
                       _result.Response = athleteProfile;
                       _result.Message = CustomConstants.DetailsGetSuccessfully;
                    }
                    else
                    {
                        
                        for (int i = 0; i < cnt; i++)
                        {
                            athleteProfile = GetAthleteProfile(emailaddress, athleteSports[i].SportId, logintype);
                            _result.Status = Utility.CustomResponseStatus.ExistsInOtherSport;
                            _result.Response = athleteProfile;
                            _result.Message = CustomConstants.DetailsGetSuccessfully;
                            if (athleteProfile.Id != 0)
                                break;
                        }
                    }
                }
                 
                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

      
    }
}
