﻿using LemonaidWrapperAPI.Models;
using System;
using System.Collections.Generic; 
using System.Web.Http; 
using System.Net.Http;
using System.Net.Http.Headers;

namespace LemonaidWrapperAPI.Controllers
{
    public class UserController : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse AddUser(UserDTO userDto)
        {

            try
            {
              
                var usrId = DAL.AddUser(userDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = usrId;
                _result.Message = CustomConstants.UserAdded;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }



        [HttpPost]
        public Utility.CustomResponse AddUserSport(UserSportsDTO userSportsDto)
        {

            try
            {
                var sports = DAL.AddUserSports(userSportsDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = sports;
                _result.Message = CustomConstants.UserSportAdded;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }



        [HttpGet]
        public Utility.CustomResponse CheckUser(string emailaddress, int logintype,string userType)
        { 
            try
            {
                //bool isexists = false;
                int sportId = 0;
                List<SportsDTO> userSports = DAL.GetUserSports(emailaddress, logintype, userType);
                foreach (var i in userSports)
                {
                    if (i.Active == 1)
                        sportId =i.Id;
                }
                int cnt = userSports.Count;
                if (cnt == 0)
                {
                    if (userType == UserTypes.CollegeCoach)
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Response = 0;
                        _result.Message = CustomConstants.DetailsGetSuccessfully;
                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.NewUser;
                        _result.Response = 0;
                        _result.Message = CustomConstants.DetailsGetSuccessfully;
                    }
                }
                else if (cnt > 3 && userType != UserTypes.FamilyFriends)
                {

                    _result.Status = Utility.CustomResponseStatus.UserNotAllowed;
                    _result.Response = 0;
                    _result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    if (userType == UserTypes.HighSchooAthlete)
                    {
                        AthleteProfileDTO athlete = new AthleteProfileDTO();
                        AthleteProfileDTO athleteProfile = null;
                        //Get Ids From Profile
                        foreach (var i in userSports)
                        {
                            athlete =
                                (AthleteProfileDTO) GetProfileFromUserType(emailaddress, i.Id, logintype, userType);
                            i.AthleteId = athlete.Id;

                            if (i.Active == 1)
                            {
                                athleteProfile = athlete;
                                athleteProfile.AthleteSports = userSports;

                            } 
                        }
                        if (athleteProfile.Id > 0)
                        {
                            _result.Status = Utility.CustomResponseStatus.Successful;
                            _result.Response = athleteProfile;
                            _result.Message = CustomConstants.DetailsGetSuccessfully;
                        }
                        else
                        {
                            var j = DAL.DeleteUserFromWrapper(emailaddress, sportId, logintype, userType);
                            _result.Status = Utility.CustomResponseStatus.NewUser;
                            _result.Response = 0;
                            _result.Message = CustomConstants.DetailsGetSuccessfully;

                        }
                    }
                    else
                    {  
                        var profile = GetProfileFromUserType(emailaddress, sportId, logintype, userType);
                        if (profile != null)
                        {
                            _result.Status = Utility.CustomResponseStatus.Successful;
                            _result.Response = profile;
                            _result.Message = CustomConstants.DetailsGetSuccessfully;
                        }
                        else
                        {
                            var i = DAL.DeleteUserFromWrapper(emailaddress, sportId, logintype, userType);
                            _result.Status = Utility.CustomResponseStatus.NewUser;
                            _result.Response = 0;
                            _result.Message = CustomConstants.DetailsGetSuccessfully;

                        }
                    }
                     

                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }


        }

        private object GetProfileFromUserType(string emailaddress, int sportId, int logintype, string userType)
        {
            object profile = null;

            if (userType == UserTypes.HighSchooAthlete || userType == UserTypes.CollegeAthlete)
            {
                profile = new AthleteProfileDTO();
                AthleteController athl = new AthleteController();
                profile = athl.GetAthleteProfile(emailaddress, sportId, logintype);
            }
            else if (userType == UserTypes.CollegeCoach)
            {
                profile = new CoachDTO();
                profile = GetCoachProfile(emailaddress, sportId); 
            } 
            else if (userType == UserTypes.ClubCoach)
            {
                profile = new CoachDTO();
                profile = GetClubCoachProfile(emailaddress, sportId);

            }
            else if (userType == UserTypes.FamilyFriends)
            {
                profile = new FamilyFriendsDTO();
                profile = GetFamilyFriendsProfile(emailaddress, sportId);

            }

            return profile;
        }




        private object GetFamilyFriendsProfile(string emailaddress, int sportId)
        {
            AthleteController athl = new AthleteController();
            string url = athl.GetURL(sportId);
            FamilyFriendsDTO familyFriends = new FamilyFriendsDTO();

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = client.GetAsync("FamilyFriends/GetFamilyFriendsProfile?emailaddress=" + emailaddress).Result;
            if (response.IsSuccessStatusCode)
            {
                Utility.CustomResponse res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                familyFriends = athl.JsonDeserialize<FamilyFriendsDTO>(res.Response.ToString());

            }

            List<SportsDTO> sports;
            sports = DAL.GetSportsFromEmail(emailaddress, 1, UserTypes.FamilyFriends);
            familyFriends.FamilyFriendsSports = sports;

            return familyFriends;
        }

        private object GetClubCoachProfile(string emailaddress, int sportId)
        {
            AthleteController athl = new AthleteController();
            string url = athl.GetURL(sportId);
            ClubCoachDTO clubcoach = new ClubCoachDTO();

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = client.GetAsync("ClubCoach/ClubCoachProfile?emailaddress=" + emailaddress).Result;
            if (response.IsSuccessStatusCode)
            {
                Utility.CustomResponse res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                clubcoach = athl.JsonDeserialize<ClubCoachDTO>(res.Response.ToString());

            }

            List<SportsDTO> sports;
            sports = DAL.GetSportsFromEmail(emailaddress, 1, UserTypes.ClubCoach);
            clubcoach.ClubCoachSports = sports;

            return clubcoach;
        }


        public CoachDTO GetCoachProfile(string emailaddress, int sportId)
        {
            AthleteController athl = new AthleteController();
            string url = athl.GetURL(sportId);
            CoachDTO coach = new CoachDTO();

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = client.GetAsync("Coachv5/CoachProfile?emailaddress=" + emailaddress).Result;
            if (response.IsSuccessStatusCode)
            {
                Utility.CustomResponse res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                coach = athl.JsonDeserialize<CoachDTO>(res.Response.ToString());

            }

            List<SportsDTO> sports;
            sports = DAL.GetSportsFromEmail(emailaddress, 1, UserTypes.CollegeCoach);
            coach.CoachSports = sports;

            return coach;
        }


        [HttpGet]
        public Utility.CustomResponse Logout(string emailaddress, int sportId, int logintype, string userType)
        { 
            try
            {
                 DAL.Logout(emailaddress, sportId, logintype, userType);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = sportId;
                _result.Message = CustomConstants.UserLogout;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse CheckCoach(CoachProfileDTO coachProfile)
        {
            try
            {
              
                    int sportId = 0;
                    string userType = UserTypes.CollegeCoach;
                    List<SportsDTO> userSports = DAL.GetUserSports(coachProfile.Emailaddress, 0, userType);
                    foreach (var i in userSports)
                    {
                        if (i.Active == 1)
                            sportId = i.Id;
                    }
                    int cnt = userSports.Count;
                    if (cnt == 0)
                    {
                        _result.Status = Utility.CustomResponseStatus.NewUser;
                        _result.Response = 0;
                        _result.Message = CustomConstants.User_Does_Not_Exist;
                    }
                    else
                    {
                        CoachDTO Coach = Login(coachProfile.Emailaddress, coachProfile.Password, sportId);
                        if (Coach != null && Coach.Id > 0)
                        {
                            var profile = GetProfileFromUserType(coachProfile.Emailaddress, sportId, 0, userType);
                            if (profile != null)
                            {
                                _result.Status = Utility.CustomResponseStatus.Successful;
                                _result.Response = profile;
                                _result.Message = CustomConstants.DetailsGetSuccessfully;
                            }
                            else
                            {
                                var i = DAL.DeleteUserFromWrapper(coachProfile.Emailaddress, sportId, 0, userType);
                                _result.Status = Utility.CustomResponseStatus.NewUser;
                                _result.Response = 0;
                                _result.Message = CustomConstants.DetailsGetSuccessfully;

                            }
                        }
                        else
                        {
                            _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                            _result.Response = string.Empty;
                            _result.Message = CustomConstants.Password_Mismatch;
                        }

                    } 

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }


        }

        private CoachDTO Login(string emailaddress, string password, int sportId)
        {
            AthleteController atrl = new AthleteController();
            string url = atrl.GetURL(sportId);
            CoachDTO coachDTO = new CoachDTO();

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = client.GetAsync("/DuoLogin/Login?emailaddress=" + emailaddress + "&password=" + password).Result;
            if (response.IsSuccessStatusCode)
            {
                Utility.CustomResponse res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                if (res.Response == null)
                    coachDTO = null;
                else
                    coachDTO = atrl.JsonDeserialize<CoachDTO>(res.Response.ToString());

            }
            return coachDTO;
        }

    }
}
