﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LemonaidWrapperAPI
{
    public class Utility
    {
        public class CustomResponse
        {
            public CustomResponseStatus Status;
            public Object Response;
            public string Message;
        }

        public enum CustomResponseStatus
        {
            Successful,
            UnSuccessful,
            Exception,
            UnmappedUniversityUser, // Exceptional only for when coach is not mapped to the university
            PaymentInProgress, // Coach has not done the Payment
            NewUser,
            ExistsInOtherSport,
            UserNotAllowed,
           UserDoesNotExist
        }

        public enum Sports
        {
            Swimming,
            Rowing,
            Diving,
            Tennis,
            CrossCountry

        }

        public enum Builds
        {
            Production,
            Test,
            Staging 

        }
      
    }

    public static class UserTypes
    {

        public static string HighSchooAthlete = "HS";
        public static string CollegeCoach = "CC";
        public static string ClubCoach = "ClC";
        public static string CollegeAthlete = "CA";
        public static string FamilyFriends = "FFF";
    }

    public static class CustomConstants
    { 
        public static string DetailsGetSuccessfully = "Data retrieved successfully";
        public static string NoRecordsFound = "No records found"; 
        public static string AthleteAdded = "Athlete added successfully";
        public static string AthleteSportAdded = "Athlete sport added successfully"; 
        public static string UserAdded = "User added successfully";
        public static string UserSportAdded = "User sport added successfully";
        public static string UserLogout = "User logged out successfully";
        public static string Password_Mismatch = "Incorrect username / password"; 
        public static string User_Does_Not_Exist = "User does not exist"; 
        public static string ForgotPassword_successfully = "Mail sent successfully to registered email address";  
        public static string Status_Added = "Status added successfully";
        public static string Chat_Added = "Chat added successfully";
        public static string Already_Exists = "Record already exists";
        public static string Match_Removed = "Match removed successfully";
        public static string Login_successfully = "Login Successfully";
        public static string EndAd_NewUser = "User does not exist";
        public static string User_Exists_Already = "User already exists. Please login with the given credentials.";
        public static string Ad_Uploaded = "Ad posted successfully"; 
        public static string ResetPassword_Failed = "Resetting password has been failed.Try again!";
        public static string ResetPassword = "Password has been reset successfully";
        public static string Video360_Added = "Coach Video360Type Added Successfully";
        public static string Video_Deleted_Successfully = "Deleted successfully";
        public static string VideoName_Updated_Successfully = "Video name updated successfully";
    }
}